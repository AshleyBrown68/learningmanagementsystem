package orders;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class ErrorPackage. which represents whether an order has an error in it. the string represents
 *  the errorMessage, and the boolean represents if there is an error or not
 */
public class ErrorPackage implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5460517322854389182L;

	/** The is there an error */
	private ErrorLocation location;

	/** The error message. */
	private String errorMessage;
	
	public ErrorLocation getLocation() {
		return location;
	}

	public void setLocation(ErrorLocation location) {
		this.location = location;
	}

	

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the new error message
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Instantiates a new error package.
	 */
	public ErrorPackage(String errorMessage, ErrorLocation location) {
		this.errorMessage=errorMessage;
		this.location=location;
	}

}
