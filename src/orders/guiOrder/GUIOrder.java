package orders.guiOrder;

import orders.SystemOrder;
import view.View;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIOrder.
 */
public abstract class GUIOrder extends SystemOrder {
	View view;
	
	/**
	 * Instantiates a new GUI order.
	 */
	public GUIOrder() {
		super();
	}
	
	public void setView(View v) {
		view = v;
	}
}
