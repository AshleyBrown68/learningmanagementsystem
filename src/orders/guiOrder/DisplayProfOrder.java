package orders.guiOrder;

import view.View;
import view.professor.panelNames;


/**
 * The Class DisplayProfOrder.
 * Starts the prof GUI if a professor logs in.
 * @author Ashley
 */
public class DisplayProfOrder extends GUIOrder {

	
	/** The prof ID. */
	private String profID;
	
	/**
	 * Instantiates a new display prof order.
	 *
	 * @param id the id
	 */
	public DisplayProfOrder(String id) {
		profID = id;
	}

	/* (non-Javadoc)
	 * @see orders.SystemOrder#execute()
	 */
	//	View view;
	@Override
	public void execute() {
//		System.out.println(view);
		view.displayProfessor(profID);
	}

}
