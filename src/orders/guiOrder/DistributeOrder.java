package orders.guiOrder;

import java.util.ArrayList;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;

/**
 * requests for the router. it is a request for the router to distribute a
 * message to each of its receivers 
 * @author brere
 */

public class DistributeOrder extends GUIOrder {

	
	/** The receiver list. */
	private ArrayList<DataReceiver> receiverList;
	
	/** The message. */
	private MessageForClient message;

	/**
	 * creates a new distributeReqeust, accepting a receiverList of the Receivers to
	 * the servers output.
	 *
	 * @param receiverList the list of receivers who will receive data from the server
	 */
	public DistributeOrder(ArrayList<DataReceiver> receiverList) {
		this.receiverList = receiverList;
		message = new MessageForClient(null, null);
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(MessageForClient message) {
		this.message = message;
	}

	/* (non-Javadoc)
	 * @see orders.SystemOrder#execute()
	 */
	public void execute() {
		if (super.isError()) {
			message.setMessage(super.getFirstError().getErrorMessage());
		}

		for (int i = 0; i < receiverList.size(); i++) {
			receiverList.get(i).receive(message);
		}
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public ArrayList<DataReceiver> getList() {
		return receiverList;
	}

	/**
	 * Gets the message.
	 * @return the message
	 */
	public MessageForClient getMessage() {
		return message;
	}

}
