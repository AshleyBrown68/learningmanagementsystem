package orders;

import java.util.ArrayList;


/**
 * The Class SystemOrder which is meant to represent any order to either the gui system, or the server system.
 * this exists to allow the genericization of most server functions
 */
public abstract class SystemOrder {
	/**
	 * satatic int for automatic id creating
	 */
private  static int nextID=1;
	/** The Constant serialVersionUID. */

	/** The error package for the system order, which represents if there has been an error before*/
	private ArrayList<ErrorPackage> errorLog;
/**
 * the id number of the order
 */
	private int id;
	
	/**
	 * Instantiates a new system order.
	 */
	public SystemOrder() {
		this.id=nextID++;
		errorLog = new  ArrayList<ErrorPackage>();
	}

	/**
	 * Execute, which executes the "main" of the order. This will do what the order wants to do.
	 */
	public abstract void execute();

	/**
	 * adds a new error to the error log
	 * 
	 * @param errorMessage the new error
	 * @param gui 
	 */
	public void addError(String errorMessage, ErrorLocation location) {
		 errorLog.add(new ErrorPackage(errorMessage,location));
	}

	/**
	 * Gets the first error that occured
	 * @return the error
	 */
	public ErrorPackage getFirstError() {
		if(errorLog.size()==0) {
			return null;
		}
		return errorLog.get(0);
	}
	public Boolean isError() {
		return errorLog.size()!=0;
	}

	public int getId() {
		return id;
	}


	
}
