package orders.databaseOrders;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import model.datafeildStuff.DataFeildList;
import model.labels.DataLabel;
/** order for the database to delete one peice of data from the database  */
public class DeleteOrder implements DataOrder {	
	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant name. */
	private static final String name = "deletion";
	
	/** The list which will be inserted into the database */
	private DataFeildList list;
	
	/** The statement. which it will execute to insert into the database*/
	private PreparedStatement statement;
	public Connection jdbc_connection;
	/* (non-Javadoc)
	 * @see orders.databaseOrders.DataOrder#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * executes the order to delete from the database
	 * follows the same logic as search, so the programmer has to be careful not to delete a lot of things by accident
	 * @return the person being deleted
	 */
	@Override
	public ArrayList<DataFeildList> execute() {
		if (list == null) {
			throw new IllegalArgumentException("error, the programer forgot to add the list to the order");
		}
		if (this.jdbc_connection  == null) {
			throw new IllegalArgumentException("error, the programer forgot to add the preparedstatement to the order");
		}
		delete();
		ArrayList<DataFeildList> array=new ArrayList<DataFeildList>();
		array.add(list);
		return array;
		
	}
	/**
	 * deletes a row of data from the database
	 */
	private void delete() {
		
		DataLabel top=list.get(0).getLabel();
		String sql = "DELETE FROM " + top.getTableName() + " WHERE ";
		
		String nextDataEntry="'"+list.get(0).getData()+"'";
		
		sql += list.get(0).getLabel().getColumnName() + "= " + nextDataEntry;
		
		for (int i = 1; i < list.size(); i++) {
			nextDataEntry="'"+list.get(i).getData()+"'";
			sql += " AND " + list.get(i).getLabel().getColumnName() + " = " + nextDataEntry;
		}
		
		
		try {
			System.out.println(sql);
			statement = jdbc_connection.prepareStatement(sql);
			
			statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("error, we were unable to modify your data in the table.  make sure that it already exists");
		}			
		return;
	}

	/* (non-Javadoc)
	 * @see orders.databaseOrders.DataOrder#setData(model.datafeildStuff.DataFeildList)
	 */
	@Override
	public void setData(DataFeildList list) {
		this.list = list;

	}

	/* (non-Javadoc)
	 * @see orders.databaseOrders.DataOrder#setStatement(java.sql.PreparedStatement)
	 */
	@Override
	public void setConnection(Connection connection) {
		this.jdbc_connection = connection;
	}
	
	/* (non-Javadoc)
	 * @see orders.databaseOrders.DataOrder#getList()
	 */
	@Override
	public DataFeildList getList() {
		return list;
	}	

}
