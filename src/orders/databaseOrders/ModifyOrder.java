package orders.databaseOrders;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import model.datafeildStuff.DataFeildList;
import model.labels.DataLabel;

/**class which an order for the database to modify the data of one user 
 * REQUIRES an id of the person being modified*/
public class ModifyOrder implements DataOrder {	
	

		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = 1L;
		
		/** The Constant name. */
		private static final String name = "modification";
		
		/** The list which will be inserted into the database */
		private DataFeildList list;
		
		/** The statement. which it will use to modify the database*/
		private PreparedStatement statement;
		public Connection jdbc_connection;
		/* (non-Javadoc)
		 * @see orders.databaseOrders.DataOrder#getName()
		 */
		@Override
		public String getName() {
			return name;
		}

		/**
		 * executes the order to modify the database
		 * @return the person being inserted
		 */
		@Override
		public ArrayList<DataFeildList> execute() {
			if (list == null) {
				throw new IllegalArgumentException("error, the programer forgot to add the list to the order");
			}
			if (this.jdbc_connection  == null) {
				throw new IllegalArgumentException("error, the programer forgot to add the preparedstatement to the order");
			}
			modify();
			ArrayList<DataFeildList> array=new ArrayList<DataFeildList>();
			array.add(list);
			return array;
			
		}
		/** modifies a row of data in the database  */
		private void modify() {
			
			DataLabel top=list.get(0).getLabel();
			String sql = "UPDATE " + top.getTableName() + " SET ";
		
			for(int i=0;i<list.size()-2;i++) {
				sql+=" "+list.get(i).getLabel().getColumnName()+"= '"+list.get(i).getData()+"',";
			}
			sql+=" "+list.get(list.size()-1).getLabel().getColumnName()+"= '"+list.get(list.size()-1).getData()+"'";

			//gets the label of the primary key.  ASSUMES primary key HAS A COLUMN NUBMER OF 1.
			DataLabel primaryKeyLabel=top.getLabelBasedOnColumnNumber(1);
			
			sql+=" WHERE "+primaryKeyLabel.getColumnName()+"= '" +list.get(primaryKeyLabel).getData()+"'";
			System.out.println(sql);
			try {
				statement = jdbc_connection.prepareStatement(sql);
				
				statement.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
				throw new IllegalArgumentException("error, we were unable to modify your data in the table.  make sure that it already exists");
			}			
			return;
		}

		/* (non-Javadoc)
		 * @see orders.databaseOrders.DataOrder#setData(model.datafeildStuff.DataFeildList)
		 */
		@Override
		public void setData(DataFeildList list) {
			this.list = list;

		}

		/* (non-Javadoc)
		 * @see orders.databaseOrders.DataOrder#setStatement(java.sql.PreparedStatement)
		 */
		@Override
		public void setConnection(Connection connection) {
			this.jdbc_connection = connection;
		}
		
		/* (non-Javadoc)
		 * @see orders.databaseOrders.DataOrder#getList()
		 */
		@Override
		public DataFeildList getList() {
			return list;
		}	
	
}
