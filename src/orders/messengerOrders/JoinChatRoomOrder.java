package orders.messengerOrders;

import messenger.MessageRoomUserInfo;
import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;

// TODO: Auto-generated Javadoc
/**
 * The Class JoinChatRoomOrder, which is the order for a user to join a chat room.  
 */
public class JoinChatRoomOrder extends MessengerOrder {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5939351755106790219L;

	/**
	 * Instantiates a new join chat room order.
	 *
	 * @param messege the messege, which holds the information of the user who wants to enter the chat room.  
	 */
	public JoinChatRoomOrder(DataFeildList messege) {
		super(messege);
	}

	/**
	 * executes the order, ie makes the user join the chat room
	 */
	@Override
	public void execute() {
		try {
			MessageRoomUserInfo info = new MessageRoomUserInfo(super.getBox().getSender(), super.getDistributeID(),
					super.getMessegeContentList().get(MessageLabel.FROM).getData());

			super.getBox().getMessager().joinRoom(info,
					super.getMessegeContentList().get(MessageLabel.CLASSID).getData());
		} catch (IllegalArgumentException e) {
			super.handleError(e);
		}
	}

}
