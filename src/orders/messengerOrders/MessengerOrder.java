package orders.messengerOrders;

import java.util.ArrayList;

import ServerOrder.ServerOrder;
import model.datafeildStuff.DataFeildList;

// TODO: Auto-generated Javadoc
/**
 * The Class MessengerOrder, which is any order for the server, which needs the messenger.  
 * it has some constants for ease of use.  
 */
public abstract class MessengerOrder extends ServerOrder{
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8702840537668065326L;
	
	/** The messege. */
	private DataFeildList messege;
	
	/**
	 * Instantiates a new messenger order.
	 *
	 * @param messege the messege
	 */
	public MessengerOrder(DataFeildList messege) {
		super(-1);
		this.messege=messege;
	}

	/**
	 * Gets the messege content list.
	 *
	 * @return the messege content list
	 */
	protected DataFeildList getMessegeContentList() {
		return messege;
	}

	/**
	 * In the case where there is an error while sending/receiving messages, this handles it
	 *
	 * @param e the error which will be handled
	 */
	protected void handleError(IllegalArgumentException e) {
		ArrayList<DataFeildList> list=new ArrayList<DataFeildList>();
		list.add(getMessegeContentList());

		forClient.getMessage().setList(list);
		forClient.getMessage().setMessage(e.getMessage());
		forClient.setDistributeDelete(false);
		
		super.getBox().getSender().send(forClient);
	}

	
	
}
