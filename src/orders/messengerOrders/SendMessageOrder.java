package orders.messengerOrders;

import model.datafeildStuff.DataFeildList;

// TODO: Auto-generated Javadoc
/**
 * this is an order for the messenging service used on the server side. it tells
 * the server to send a message to the classmates/professor
 * 
 * @author brere
 */
public class SendMessageOrder extends MessengerOrder {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3867620070205000692L;

	/**
	 * Instantiates a new send message order.
	 *
	 * @param messege the messege
	 */
	public SendMessageOrder(DataFeildList messege) {
		super(messege);
	}

	/**
	 * executes the server order, ie it sends a message to all of the classmates
	 */
	@Override
	public void execute() {
		try {
			super.getBox().getMessager().sendMessage(super.getMessegeContentList());
		} catch (IllegalArgumentException e) {
			super.handleError(e);
			return;
		}
	}

}
