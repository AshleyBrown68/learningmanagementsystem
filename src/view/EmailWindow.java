package view;

import javax.swing.JDialog;
import javax.swing.JPanel;

import control.dataSending.ClientPackager;
import view.professor.ProfessorPanel;
import view.student.StudentPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class EmailWindow.
 * @author Ashley
 */
public class EmailWindow {


	/** The window. */
	private JDialog window;

/** The components. */
//	private JPanel panel;
	private EmailComponents components;
	
	/** The router data. */
	private RouterData routerData;
	/** The packager. */
	private ClientPackager packager;

	/**
	 * The ids.
	 * 
	 * ids[0] = user id 
	 * ids[1] = course id
	 * 
	 */
	private String[] ids;

	
	/**
	 * Instantiates a new email window.
	 *
	 * @param data the data
	 * @param p the p
	 * @param i the i
	 * @param type the type
	 */
	public EmailWindow(RouterData data, ClientPackager p, String[] i, String type) {
		routerData = data;
		packager = p;
		ids = i;
		
		window = new JDialog();
		window.setSize(400, 400);
		components = new EmailComponents(routerData, packager, ids, type);
		window.add(components);
		window.setVisible(true);
	}


//	@Override
//	public void setup(String[] arguments) {
//		ids = arguments;
//		
//	}

}
