package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import control.clientDataReceiving.GUIRouter;
import control.clientDataReceiving.Router;
import control.dataSending.ClientPackager;
import model.serverStuff.serverCommunication.SocketSender;
import view.professor.Professor;
import view.student.Student;





/**
 * The Class View.
 * @author Ashley
 */
public class View {
	
	//REPLACE WITH THE FOLDER ON YOUR COMPUTER BEFORE PRESENTATION
//	public static final String IMAGE_FOLDER = "C:\\Users\\Ashley\\Documents\\UofC\\ENSF409\\Final Project\\finalprojectensf409_ashley_kevin_rebecccca\\";
//	public static final String IMAGE_FOLDER = "";
	
	/** The Constant LOGIN_BACKGROUND_COLOUR. */
	public static final Color LOGIN_BACKGROUND_COLOUR = new Color (255, 153, 232);
	
	/** The Constant BACKGROUND_COLOUR. */
	public static final Color BACKGROUND_COLOUR = new Color(255, 204, 255);
	
	/** The Constant STUDENT_BACKGROUND_COLOR. */
	public static final Color STUDENT_BACKGROUND_COLOR = new Color(255, 86, 168);
	
	/** The Constant WINDOW_BACKGROUND_COLOR. */
	public static final Color WINDOW_BACKGROUND_COLOR = new Color(204, 13, 124);
	
	public static Dimension screenSize;

	/** The frame. */
	private JFrame frame;
	
	/** The current GUI. */
	private JPanel currentGUI;

/** The sender. */
	private SocketSender sender;
	
	/** The router. */
	private RouterData data;
	
	
	
	/**
	 * Sets up the GUI and opens the login panel when the program is started.
	 *
	 * @param guirouter the guirouter
	 * @param router the router
	 * @param sender the sender
	 */
	public void setup(GUIRouter guirouter, SocketSender sender) {
		 screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		data = new RouterData(guirouter);
		this.sender=sender;
//		this.guirouter = guirouter;
//		this.router = router;
		frame = new JFrame();
		closeFrame(frame);
		displayLogin();	
	}
	
	/**
	 * Display login.
	 */
	public void displayLogin() {
//		frame.remove(currentGUI);
		
		frame.setSize(screenSize.width/4, screenSize.height/2);
		currentGUI = new Login(data.getGuirouter(),new ClientPackager(sender));
		frame.add(currentGUI);
		frame.setVisible(true);
	}
	
	/**
	 * Creates and displays the professor GUI.
	 *
	 * @param profID the prof ID
	 */
	public void displayProfessor(String profID) {
//		frame.remove(currentGUI);
		frame.dispose();
		frame = new JFrame();
		closeFrame(frame);
		
		frame.setSize(screenSize.width/2, screenSize.height/2);
		
//		
		try {
		currentGUI = new Professor(frame.getContentPane(), data, profID,new ClientPackager(sender));
		}catch(Exception e) {
			System.out.println("error creating new professor");
			e.printStackTrace();
		}
		
		try {
			frame.add(currentGUI);
		}catch(Exception e) {
			System.out.println("exception when adding panel to frame");
		}
		frame.setVisible(true);
		
		
		
	}
	
	/**
	 * Creates and displays the student GUI.
	 *
	 * @param studentID the UserID of the student using the GUI
	 */
	public void displayStudent(String studentID) {
//		frame.remove(currentGUI);
//		System.out.println("DISPLAY");
		frame.dispose();
		frame = new JFrame();
		closeFrame(frame);
		frame.setSize(screenSize.width/2, screenSize.height/2);
		try {
		currentGUI = new Student(frame.getContentPane(), data, studentID,new ClientPackager(sender));
		}catch(Exception e) {
			System.out.println("error creating new student");
			e.printStackTrace();
		}
//		System.out.println("point ?");
		try {
			frame.add(currentGUI);
		}catch(Exception e) {
			System.out.println("exception when displaying student ");
			e.printStackTrace();
		}
		frame.setVisible(true);
		
	}
	
	/**
	 * Gets the current GUI.
	 *
	 * @return the current GUI
	 */
	public JPanel getCurrentGUI() {
		return currentGUI;
	}
	
	/**
	 * Sets up the window closing listener.
	 *
	 * @param frame the frame
	 */
	private void closeFrame(JFrame frame) {

		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener( new WindowAdapter()
		{
		    public void windowClosing(WindowEvent e)
		    {
		        JFrame frame = (JFrame)e.getSource();
		 
		        int result = JOptionPane.showConfirmDialog(
		            frame,
		            "Are you sure you want to exit the application?",
		            "Exit Application",
		            JOptionPane.YES_NO_OPTION);
		 
		        if (result == JOptionPane.YES_OPTION) {
		        	
		            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        }
		    }
		});
	}
	
	
}
