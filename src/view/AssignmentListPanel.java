package view;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;


/**
 * The Class AssignmentListPanel.
 * Holds a list of all assignments for a specific course.
 * @author Ashley
 */
public class AssignmentListPanel extends JPanel{
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6982023600209736199L;
	
	/** The layout. */
	protected BorderLayout layout;
	
	/** The assignments. */
	protected ResultsList<ListDataElementAssignment> assignments;
	
	/** The send email. */
	protected JButton sendEmail;
	
	/**
	 * Instantiates a new assignment list panel.
	 */
	public AssignmentListPanel() {
		layout = new BorderLayout();
		setLayout(layout);
		
		assignments = new ResultsList<ListDataElementAssignment>();
		sendEmail = new JButton("Send Email");
		
		add(assignments, layout.CENTER);
	}
	
	//Add actionListeners 
	
	/**
	 * Adds the send email action listener.
	 *
	 * @param al the al
	 */
	public void addSendEmailActionListener(ActionListener al) {
		sendEmail.addActionListener(al);
	}

	
	
//	Getters
	
	
	/**
 * Gets the assignments.
 *
 * @return the assignments
 */
public ResultsList<ListDataElementAssignment> getAssignments() {
		return assignments;
	}

	/**
	 * Gets the send email.
	 *
	 * @return the send email
	 */
	public JButton getSendEmail() {
		return sendEmail;
	}
}
