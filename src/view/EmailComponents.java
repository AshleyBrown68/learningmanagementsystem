package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.EmailRequestFromProfGUItoServerGUIOrder;
import control.dataGUIOrders.EmailRequestFromStudentGUIOrder;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
import model.labels.MessageLabel;
import model.labels.StudentEnrollmentLabel;

/**
 * The Class EmailComponents.
 * Sets up the window to send new email.
 * @author Ashley
 */
public class EmailComponents extends JPanel{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The subject. */
	private Field subject;
	
	/** The editor pane. */
	private JEditorPane editorPane;
	
	/** The row. */
	private int row;
	
	/** The send. */
	private JButton send;
	
	/** The router data. */
	private RouterData routerData;
	/** The packager. */
	private ClientPackager packager;

	/**
	 * The ids.
	 * 
	 * ids[0] = user id 
	 * ids[1] = course id
	 */
	private String[] ids;
	
	/** The type. */
	private String type;

	
	
	/**
	 * Instantiates a new email components.
	 *
	 * @param data the data
	 * @param p the p
	 * @param i the i
	 * @param type the type
	 */
	public EmailComponents(RouterData data, ClientPackager p, String[] i, String type) {
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
		
		routerData = data;
		packager = p;
		ids = i;
		this.type = type;
		
		this.setBackground(View.WINDOW_BACKGROUND_COLOR);
		row = 0;
		
		subject = new Field(this, row++, "Subject: ");
		editorPane = new JEditorPane();
//		editorPane.setSize(30, 8);
		//Put the editor pane in a scroll pane.
		JScrollPane editorScrollPane = new JScrollPane(editorPane);
		editorScrollPane.setVerticalScrollBarPolicy(
		                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		editorScrollPane.setPreferredSize(new Dimension(250, 145));
		editorScrollPane.setMinimumSize(new Dimension(10, 10));
		
		send = new JButton("Send");
		send.addActionListener(new SendListener());
		
		gbc.gridy = row++;
		row++;
		gbc.gridheight = 2;
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		add(editorScrollPane, gbc);
		
		gbc.gridheight = 1;
//		gbc.gridwidth = 1;
		gbc.gridy = row++;
		add(send, gbc);
	}

	/**
	 * The listener interface for receiving send events.
	 * The class that is interested in processing a send
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addSendListener<code> method. When
	 * the send event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SendEvent
	 */
	public class SendListener implements ActionListener, DataReceiver{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);
			if(type.equalsIgnoreCase("P")) {
				DataFeildList list = new DataFeildList();
				list.add(new DataFeild(StudentEnrollmentLabel.COURSE_ID,ids[1]));
				
				String s = subject.getText().getText();
				String m = editorPane.getText();
				
				EmailRequestFromProfGUItoServerGUIOrder order = new EmailRequestFromProfGUItoServerGUIOrder(list, packager, s, m);
				routerData.getGuirouter().executeOrder(order);
			}
			else if(type.equalsIgnoreCase("S")) {
				DataFeildList list = new DataFeildList();
				list.add(new DataFeild(CourseLabel.ID,ids[1]));
				
				String s = subject.getText().getText();
				String m = editorPane.getText();
				EmailRequestFromStudentGUIOrder order = new EmailRequestFromStudentGUIOrder(list, packager, s, m);
				routerData.getGuirouter().executeOrder(order);
			}
			
			
			
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient list) {
			JOptionPane.showMessageDialog(null, "Email Sent", "Email", JOptionPane.PLAIN_MESSAGE );
			
			
		}
		
		
	}
}