package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.GUIRouter;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.LoginEnterListener;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.datafeildStuff.UserType;
import model.labels.UserLabel;
import orders.databaseOrders.SearchOrder;
import orders.guiOrder.DisplayProfOrder;
import orders.guiOrder.DisplayStudentOrder;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * The Class Login.
 * @author Ashley
 */
public class Login extends JPanel implements DataSender, DataReceiver{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The username. */
	private Field username;
	
	/** The password. */
	private JLabel password;
	
	/** The password text. */
	private JPasswordField passwordText;
	
	/** The enter. */
	private JButton enter;
	
	/** The gbc. */
	private GridBagConstraints gbc;
	
	/** The row. */
	private int row;
	
	/** The packager. */
	private ClientPackager packager;
	
	/** The router. */
	private GUIRouter router;
	
	
	
	
	/**
	 * Instantiates a new login.
	 *
	 * @param router the router
	 * @param p the p
	 */
	public Login(GUIRouter router,ClientPackager p) {
		this.setBackground(View.LOGIN_BACKGROUND_COLOUR);
		packager=p;
		packager.addReceiver(this);
		this.router = router;
		
		this.setLayout(new GridBagLayout());
		setupGBC();
		
		BufferedImage myPicture;
		try {
//			myPicture = ImageIO.read(new File(View.IMAGE_FOLDER + "Dead2Life.png"));
			myPicture = ImageIO.read(new File("images\\Desire2LoveNoBackground.png"));
			JLabel picLabel = new JLabel(new ImageIcon(myPicture.getScaledInstance(100, 100, Image.SCALE_FAST)));
			gbc.gridy = row++;
			gbc.gridwidth = 2;
			gbc.gridx = 0;
			
			add(picLabel, gbc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		gbc.gridwidth = 1;
		
		username = new Field(this, row++, "Email");
		enter = new JButton("submit");
		row++;
		row++;
		gbc.gridy = row++;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		gbc.gridx = 1;
		//		password = new Field(this, row++, "Password");
				password = new JLabel("Password");
				GridBagConstraints gbc_password = new GridBagConstraints();
				gbc_password.insets = new Insets(0, 0, 5, 5);
				gbc_password.gridx = 0;
				gbc_password.gridy = 2;
				this.add(password, gbc_password);
		passwordText = new JPasswordField();
		passwordText.setColumns(15);
		GridBagConstraints gbc_passwordText = new GridBagConstraints();
		gbc_passwordText.insets = new Insets(0, 0, 5, 5);
		gbc_passwordText.gridx = 1;
		gbc_passwordText.gridy = 2;
		this.add(passwordText, gbc_passwordText);
		
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = row++;
		this.add(enter, gbc);
		
		addActionListener();
	}
	
	
	
	/**
	 * Setup GBC.
	 */
	private void setupGBC() {
		gbc = new GridBagConstraints();
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
	}


	
	/**
	 * Adds the action listener.
	 */
	public void addActionListener() {
		enter.addActionListener(new EnterActionListener());
	}
//	
//	public Field getUsername() {
//		return username;
//	}
//
//	public JLabel getPassword() {
//		return password;
//	}
//
//	public JPasswordField getPasswordText() {
//		return passwordText;
//	}
//
//	public JButton getEnter() {
//		return enter;
//	}



	/* (non-Javadoc)
 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
 */
@Override
	public void receive(MessageForClient message) {
		
		ArrayList<DataFeildList>results=message.getList();
//		System.out.println(results);
		if(results.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Invalid Username or Password", "Error", JOptionPane.PLAIN_MESSAGE );
		}
		
		else if (results.get(0).get(UserLabel.TYPE).getData().equals(UserType.PROFESSOR.getType())) {

			String profID = results.get(0).get(UserLabel.ID).getData();
			DisplayProfOrder order = new DisplayProfOrder(profID);
			router.executeOrder(order);
			
		}
		
		
		else if (results.get(0).get(UserLabel.TYPE).getData().equals(UserType.STUDENT.getType())) {
			
			String studentID = results.get(0).get(UserLabel.ID).getData();	
			DisplayStudentOrder order = new DisplayStudentOrder(studentID);
//			System.out.println("STUDENT");
			router.executeOrder(order);
			
		}
		
		else {
			JOptionPane.showMessageDialog(null, "Not in database as a professor or student", "Error", JOptionPane.PLAIN_MESSAGE );
		}
		
		
	
	}


	/**
	 * The listener interface for receiving enterAction events.
	 * The class that is interested in processing a enterAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addEnterActionListener<code> method. When
	 * the enterAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see EnterActionEvent
	 */
	class EnterActionListener implements ActionListener{
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent arg0) {
//			
//			call series of orders
			DataFeildList data = send();
			
			SearchGUIOrder search = new SearchGUIOrder(data,packager);
			
			
//			How to add the prepared statement?
			router.executeOrder(search);
//			System.out.println("SEARCH SENT");
			
			
		}
	}
	
	
	/* (non-Javadoc)
	 * @see control.dataSending.DataSender#send()
	 */
	public DataFeildList send() {
		DataFeildList list=new DataFeildList();

		list.add(new DataFeild(UserLabel.EMAIL, username.getText().getText()));
		list.add(new DataFeild(UserLabel.PASSWORD, passwordText.getText()));
		
		return list;
	}
	
	

}
