package view.student;

import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.InsertGUIOrder;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataGUIOrders.SendFileToServerGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import model.labels.CourseLabel;
import model.labels.SubmissionLabel;
import orders.guiOrder.ChangePanelOrder;
import orders.guiOrder.DisplayProfOrder;
import view.Field;
import view.RouterData;
import view.View;
import view.professor.panelNames;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;


/**
 * The Class AddSubmissionWindow.
 * Creates a window for a student to upload a document and submit
 * it as a submission for a particular assignment.
 * @author Ashley
 */
public class AddSubmissionWindow implements DataSender, StudentPanel {

	/** The window. */
	private JDialog window;
	/** The panel. */
	private JPanel panel;
	
	/**  The layout details. */
	private GridBagConstraints gbc;

	/** The choose file button. */
	private JButton chooseFile;
	/** The selected file. */
	private File selectedFile;
	/** The upload button. */
	private JButton upload;

	/** The router. */
	private RouterData router;
	/** The packager. */
	private ClientPackager packager;
	/**
	 * The ids.
	 * 
	 * ids[0] = studentID ids[1] = courseID ids[2] = assignmentID
	 */
	private String[] ids;

	/**
	 * Instantiates a new window for uploading submissions.
	 *
	 * @param router
	 *            the router
	 * @param p
	 *            the packager
	 * @param ids
	 *            the ids
	 */
	public AddSubmissionWindow(RouterData router, ClientPackager p, String[] ids) {

		this.router = router;
		packager = p;
		packager.clearReceivers();
		// packager.addReceiver(this);
		this.ids = ids;
		// System.out.println(profID);

		window = new JDialog();
		window.setSize(400, 200);

		panel = new JPanel();
		panel.setBackground(View.WINDOW_BACKGROUND_COLOR);
		panel.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;

		chooseFile = new JButton("Add a File");
		addChooseFileListener();

		// dueDate = new Field("Due Date: ");

		// active = new JCheckBox("Active");

		upload = new JButton("Upload");
		upload.addActionListener(new UploadListener());

		//

		// panel.add(new Label("testing add assignment window"));
		gbc.gridy = 0;
		gbc.gridx = 0;
		panel.add(chooseFile, gbc);

		gbc.gridx = 1;
		panel.add(upload, gbc);

		window.add(panel);
		window.setVisible(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see view.student.StudentPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		// TODO Auto-generated method stub
		ids = arguments;

	}

	// public void displayGrade() {
	// packager.clearReceivers();
	// packager.addReceiver(this);
	//
	// DataFeildList list = new DataFeildList();
	// list.add(new DataFeild(SubmissionLabel.STUDENT_ID, ids[0]));
	// list.add(new DataFeild(SubmissionLabel.ASSIGNMENT_ID, ids[2]));
	//
	// SearchGUIOrder search = new SearchGUIOrder(list, packager);
	// router.getGuirouter().executeOrder(search);
	// }
	// @Override
	// public void receive(MessageForClient list) {
	// int recent = list.getList().size();
	// String mark =
	// list.getList().get(recent).get(SubmissionLabel.SUBMISSION_GRADE).getData();
	//
	// }

	/**
	 * Adds the choose file listener.
	 */
	public void addChooseFileListener() {
		chooseFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// open file explorer
				// TO DO: ADD ERROR CHECKING
				File file = new File("C:");
				JFileChooser fileBrowser = new JFileChooser();
				if (fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					selectedFile = fileBrowser.getSelectedFile();
				}

				// Setup if time - good file explorer
				// Desktop desktop = Desktop.getDesktop();
				// try {
				// desktop.open(file);
				// } catch (IOException e) {
				// System.out.println("error opening file explorer");
				// e.printStackTrace();
				// }

			}

		});
	}

	/**
	 * Escape path.
	 *
	 * @param path the path
	 * @return the string
	 */
	public static String escapePath(String path) {
		return path.replace("\\", "\\\\");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see control.dataSending.DataSender#send()
	 */
	@Override
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();
		//
		list.add(new DataFeild(SubmissionLabel.STUDENT_ID, ids[0]));
		list.add(new DataFeild(SubmissionLabel.ASSIGNMENT_ID, ids[2]));

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String time = timestamp.toString();
		System.out.println(time);
		list.add(new DataFeild(SubmissionLabel.TIMESTAMP, time));

//		File parent = selectedFile.getParentFile();
		String path = selectedFile.getAbsolutePath();
		path = escapePath(path);

		String[] nameParts = selectedFile.getName().split("\\.");
		String name = nameParts[0];
		String extension = "." + nameParts[1];

		list.add(new DataFeild(SubmissionLabel.PATH, path));
		list.add(new DataFeild(SubmissionLabel.TITLE, name + extension));
		list.add(new DataFeild(SubmissionLabel.EXTENSION, extension));
		list.add(new DataFeild(SubmissionLabel.SUBMISSION_GRADE, ""));
		list.add(new DataFeild(SubmissionLabel.COMMENTS, ""));
		//
		return list;
	}

	/**
	 * The listener interface for receiving upload events. The class that is
	 * interested in processing a upload event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addUploadListener<code> method. When the upload event
	 * occurs, that object's appropriate method is invoked.
	 *
	 * @see UploadEvent
	 */
	class UploadListener implements ActionListener, DataReceiver, panelNames {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);

			SendFileToServerGUIOrder order = new SendFileToServerGUIOrder(send(), packager);
			router.getGuirouter().executeOrder(order);

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.
		 * MessageForClient)
		 */
		@Override
		public void receive(MessageForClient list) {
			JOptionPane.showMessageDialog(null, "New Assignment Uploaded", "Message", JOptionPane.PLAIN_MESSAGE);
			window.dispose();
			// DisplayProfOrder order = new DisplayProfOrder();
			ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTS, ids);
			router.getGuirouter().executeOrder(order);

		}

	}

}
