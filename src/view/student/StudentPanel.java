package view.student;

/**
 * The Interface StudentPanel.
 * Implemented by all panels held by Student.  
 * (all panels changes using ChangePanelOrder)
 * @author Ashley
 */
public interface StudentPanel {
	
	/**
	 * Sets the up.
	 *
	 * @param arguments the new up
	 */
	public void setup(String[]arguments);
}
