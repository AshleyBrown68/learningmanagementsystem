package view.student;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
import model.labels.StudentEnrollmentLabel;
import orders.guiOrder.ChangePanelOrder;
import view.ListDataElement;
import view.ListDataElementCourse;
import view.ResultsList;
import view.RouterData;
import view.View;
import view.professor.panelNames;



/**
 * The Class StudentCourseListPanel.
 * Displays a list of all active courses the students are enrolled in.
 * @author Ashley
 */
public class StudentCourseListPanel extends JPanel implements StudentPanel, DataSender, DataReceiver{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
/** Displays the list of all courses the student is enrolled in. */
	private ResultsList<ListDataElementCourse> courses;
	
	/** The refresh. */
	private JButton refresh;
	
	/** The router data. */
	private RouterData routerData;
	
	/** The packager. */
	private ClientPackager packager;
	
	/** The ids. 
	 * 
	 * ids[0] = studentID
	 */
	private String [] ids;
	
	
	
	/**
 * Instantiates a new student course list panel.
 *
 * @param router the router
 * @param p the packager
 */
public StudentCourseListPanel(RouterData router, ClientPackager p) {
		this.setBackground(View.STUDENT_BACKGROUND_COLOR);
		BorderLayout layout = new BorderLayout();
		setLayout(layout);
		
		routerData = router;
		packager=p;
		
		
		courses = new ResultsList<ListDataElementCourse>();
		courses.addListSelectionListener(new ListActionListener());
		
		refresh = new JButton("Refresh");
		refresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setup(ids);
			}
		});
		
		
		
		
		Label title = new Label("Courses");
//		Font labelFont = title.getFont();
//		title.setFont(new Font(labelFont.getName(), Font.PLAIN, 32));
		this.add(title, layout.NORTH);
		this.add(courses, layout.CENTER);
		
		JPanel south = new JPanel();
		south.setBackground(View.STUDENT_BACKGROUND_COLOR);
		south.add(refresh);
		try {
			BufferedImage myPicture = ImageIO.read(new File("images\\heart1.png"));
			JLabel picLabel = new JLabel(new ImageIcon(myPicture.getScaledInstance(50, 50, Image.SCALE_FAST)));
			south.add(picLabel);
		} catch (IOException e) {
			e.printStackTrace();
		}
		add(south, layout.SOUTH);
		
	}
	
	/* (non-Javadoc)
	 * @see view.student.StudentPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		// TODO Auto-generated method stub
		ids = arguments;
		courses.clear();
		
		packager.clearReceivers();
		packager.addReceiver(this);
		SearchGUIOrder search = new SearchGUIOrder(send(), packager);
		routerData.getGuirouter().executeOrder(search);
		
	}

	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	@Override
	public void receive(MessageForClient message) {
//		recieve list of StudentEnrollmentLabels
//		Need to search for Course name by CourseID
		
		SecondSearch search = new SecondSearch();
		search.run(message);
		
		
	}
	
	/**
	 * The Class SecondSearch.
	 */
	class SecondSearch implements  DataReceiver{
		
		/**
		 * Run.
		 *
		 * @param message the message
		 */
		public void run(MessageForClient message) {
			
			packager.clearReceivers();
			packager.addReceiver(this);
		
//			for(DataFeildList result : message.getList()) {
//				DataFeildList list=new DataFeildList();
//				String classID = result.get(StudentEnrollmentLabel.COURSE_ID).getData();
//				list.add(new DataFeild(CourseLabel.ID, classID));
//				
//				SearchGUIOrder search = new SearchGUIOrder(list, packager);
//				routerData.getGuirouter().executeOrder(search);
//			}
			for(int i = 0; i < message.getList().size(); i++) {
				DataFeildList list=new DataFeildList();
				String classID = message.getList().get(i).get(StudentEnrollmentLabel.COURSE_ID).getData();
				list.add(new DataFeild(CourseLabel.ID, classID));
				
				SearchGUIOrder search = new SearchGUIOrder(list, packager);
				routerData.getGuirouter().executeOrder(search);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient message) {
			ArrayList<DataFeildList> list = message.getList();
			for(DataFeildList element : list) {
				String active = element.get(CourseLabel.ACTIVE).getData();
				if(active.equalsIgnoreCase("t")) {
//					System.out.println(element);
					ListDataElementCourse courseFound = new ListDataElementCourse(element);
					courses.addEntry(courseFound);
				}
				

			}
//			System.out.println("added");
			
		}

//		@Override
//		public DataFeildList send() {
//			// TODO Auto-generated method stub
//			return null;
//		}
		
	}

	/* (non-Javadoc)
	 * @see control.dataSending.DataSender#send()
	 */
	@Override
	public DataFeildList send() {
		DataFeildList list=new DataFeildList();

//		
//		list.add(new DataFeild(UserLabel.EMAIL, username.getText().getText()));
	
		list.add(new DataFeild(StudentEnrollmentLabel.STUDENT_ID, ids[0]));
		return list;
	}

	
	
	/**
	 * The listener interface for receiving listAction events.
	 * The class that is interested in processing a listAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addListActionListener<code> method. When
	 * the listAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ListActionEvent
	 */
	class ListActionListener implements ListSelectionListener, panelNames{

		/* (non-Javadoc)
		 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent e) {
			int index = courses.getListOfResults().getSelectedIndex();
			if(index != -1) {
				ListDataElementCourse selectedCourse = (ListDataElementCourse) courses.getListModel().getElementAt(index);
				String courseID = selectedCourse.getID();
				
				String[] setup = {ids[0], courseID};
//				courses.clear();
				ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTS, setup);
				routerData.getGuirouter().executeOrder(order);
			}
				
		}
	}





	
}
