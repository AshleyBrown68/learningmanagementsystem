package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.InsertGUIOrder;
import control.dataGUIOrders.messengerStuff.MessengerSendGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import model.labels.MessageLabel;
import view.professor.ProfessorPanel;
import view.student.StudentPanel;


/**
 * The Class NewMessageWindow.
 * @author Ashley
 */
public class NewMessageWindow{
	
	/** The window. */
	private JDialog window;
	
	/** The panel. */
	private JPanel panel;
//	private Field to;
/** The message. */
//	private Field from;
	private JTextArea message;
	
	/** The sending. */
	private JButton sending;
	
	/** The router data. */
	private RouterData routerData;
	/** The packager. */
	private ClientPackager packager;
	/** The ids.
	 * 	ids[0] = studentID
	 * ids[1] = courseID
	 */
	String[] ids;

	
	/**
	 * Instantiates a new new message window.
	 *
	 * @param data the data
	 * @param p the p
	 * @param allIDs the all I ds
	 */
	public NewMessageWindow(RouterData data, ClientPackager p, String [] allIDs) {
		routerData = data;
		packager=p;
		ids = allIDs;
		
		window = new JDialog();
		window.setSize(400, 400);
		
		panel = new JPanel(new GridBagLayout());
		panel.setBackground(View.WINDOW_BACKGROUND_COLOR);
		GridBagConstraints gbc = new GridBagConstraints();
		
//		to = new Field(panel, 0, "To: ");
//		from = new Field(panel, 1, "From: ");
		message = new JTextArea();
		message.setColumns(25);
		message.setRows(5);
		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		message.setBorder(loweredbevel);
		
		sending = new JButton("Send");
		
//		gbc.gridwidth = 2;
		gbc.gridheight = 2;
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
		gbc.gridy = 0;
		gbc.gridx = 0;
		panel.add(message, gbc);
		
		gbc.gridheight = 1;
		gbc.gridy = 3;
		panel.add(sending, gbc);
		sending.addActionListener(new SendListener());
		
		window.add(panel);
		window.setVisible(true);
	}
	
	/**
	 * The listener interface for receiving send events.
	 * The class that is interested in processing a send
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addSendListener<code> method. When
	 * the send event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SendEvent
	 */
	class SendListener implements ActionListener, DataSender, DataReceiver{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);
			MessengerSendGUIOrder insert = new MessengerSendGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(insert);
		}

		/* (non-Javadoc)
		 * @see control.dataSending.DataSender#send()
		 */
		@Override
		public DataFeildList send() {
			DataFeildList list = new DataFeildList();
			list.add(new DataFeild(MessageLabel.FROM,ids[0]));
			list.add(new DataFeild(MessageLabel.MESSAGE, message.getText()));
			list.add(new DataFeild(MessageLabel.CLASSID, ids[1]));
			
			
			return list;
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient list) {
			System.out.println("received");
			window.dispose();
			
		}
		
	}


}
