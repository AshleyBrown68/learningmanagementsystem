package view;

import java.awt.GridBagConstraints;
//import java.awt.Insets;
import java.io.Serializable;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

// TODO: Auto-generated Javadoc
/**
 * The Class Field.
 * @author Ashley
 * 
 */
public class Field implements Serializable{
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3315585574237591175L;

	/** The label. */
	private JLabel label;
	
	/** The text. */
	private JTextField text;
	
	/** The c. */
	private GridBagConstraints c;
	
	/**
	 * Instantiates a new field.
	 *
	 * @param panel the panel
	 * @param row the row
	 * @param name the name
	 */
	public Field(JPanel panel, int row, String name) {
		c = new GridBagConstraints();
		
		
		c.weightx = 0.5;
		c.weighty = 0.5;
		
		label = new JLabel(name);
		c.gridx = 0;
		c.gridy = row;
		panel.add(label, c);
		
		text = new JTextField();
		c.gridx = 1;
		c.gridy = row;
//		c.gridwidth = 2;
		text.setColumns(15);
		panel.add(text, c);
		c.gridwidth = 1;
	}
	
	/**
	 * Instantiates a new field.
	 *
	 * @param panel the panel
	 * @param name the name
	 */
	public Field(JPanel panel, String name) {
		label = new JLabel(name);
		text = new JTextField();
		text.setColumns(15);
		
		panel.add(label);
		panel.add(text);
	}
	
	/**
	 * Instantiates a new field.
	 *
	 * @param name the name
	 */
	public Field(String name) {
		label = new JLabel(name);
		text = new JTextField();
		text.setColumns(15);
	}
	
	/**
	 * Display.
	 *
	 * @param s the s
	 */
	public void display(String s) {
		text.setText(s);
	}
	
	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public JLabel getLabel() {
		return label;
	}

	/**
	 * Sets the label.
	 *
	 * @param label the new label
	 */
	public void setLabel(JLabel label) {
		this.label = label;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public JTextField getText() {
		return text;
	}

	/**
	 * Sets the text.
	 *
	 * @param text the new text
	 */
	public void setText(JTextField text) {
		this.text = text;
	}
	
	
	
}