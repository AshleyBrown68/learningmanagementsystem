package view.professor;


/**
 * The Interface ProfessorPanel.
 * Used by all panels that a professor will view and are part of View.
 * (will be changed using the ChangePanelOrder)
 * @author Ashley
 */
public interface ProfessorPanel {

	/**
	 * Sets up a panel for a professor to view.
	 *
	 * @param arguments the new up
	 */
	public void setup(String[] arguments);
}
