package view.professor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.ModifyGUIOrder;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import model.labels.CourseLabel;
import orders.guiOrder.ChangePanelOrder;
//import orders.guiOrder.NewEmailOrder;
import view.Field;
import view.RouterData;
import view.View;


/**
 * The Class AssignmentDetails.
 * @author Ashley
 */
public class AssignmentDetails extends JPanel implements DataReceiver, ProfessorPanel, panelNames {
	
	/** The active. */
	private JCheckBox active;
	
	/** The view submissions. */
	private JButton viewSubmissions;
	
	/** The return to course home. */
	private JButton returnToCourseHome;
	
	/** The due date. */
	private Field dueDate;

	/** The ids. */
	private String[] ids;
	// ids[0] = profid
	// ids[1] = course id
	// ids[2] = assignment id

	/** The router data. */
	private RouterData routerData;
	
	/** The packager. */
	private ClientPackager packager;

	/**
	 * Instantiates a new assignment details.
	 *
	 * @param r the r
	 * @param p the p
	 */
	public AssignmentDetails(RouterData r, ClientPackager p) {
		this.setBackground(View.BACKGROUND_COLOUR);

		routerData = r;
		packager = p;

		viewSubmissions = new JButton("View Submissions");
		viewSubmissions.addActionListener(new SubmissionsActionListener());

		returnToCourseHome = new JButton("Return to Course Home");
		returnToCourseHome.addActionListener(new ReturnActionListener());

		// change details/delete button?

		active = new JCheckBox("Active");
		active.addActionListener(new ActiveListener());
		active.setBackground(View.BACKGROUND_COLOUR);

		dueDate = new Field("Due Date: ");

		add(viewSubmissions);
		add(returnToCourseHome);
		add(active);
		add(dueDate.getLabel());
		add(dueDate.getText());
	}

	/* (non-Javadoc)
	 * @see view.professor.ProfessorPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		ids = arguments;

		packager.clearReceivers();
		packager.addReceiver(this);

		componentSetup();

	}

	/**
	 * Gets the view submissions.
	 *
	 * @return the view submissions
	 */
	public JButton getViewSubmissions() {
		return viewSubmissions;
	}

	/**
	 * Component setup.
	 */
	public void componentSetup() {
		packager.clearReceivers();
		packager.addReceiver(this);
		SearchGUIOrder search = new SearchGUIOrder(send(), packager);
		routerData.getGuirouter().executeOrder(search);
	}

	/**
	 * Send.
	 *
	 * @return the data feild list
	 */
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();

		list.add(new DataFeild(AssignmentLabel.COURSE_ID, ids[1]));
		list.add(new DataFeild(AssignmentLabel.ID, ids[2]));
		return list;
	}

	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	@Override
	public void receive(MessageForClient list) {
		String isActive = list.getList().get(0).get(AssignmentLabel.ACTIVE).getData();
		if (isActive.equalsIgnoreCase("t")) {
			active.setSelected(true);
		} else if (isActive.equalsIgnoreCase("f")) {
			active.setSelected(false);
		} else {
			System.out.println("Error reading whether course is active");
		}

		String due = list.getList().get(0).get(AssignmentLabel.DUE_DATE).getData();
		dueDate.getText().setText(due);
	}

	// ----------------------------------------------------------------------------------

	/**
	 * The listener interface for receiving submissionsAction events.
	 * The class that is interested in processing a submissionsAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addSubmissionsActionListener<code> method. When
	 * the submissionsAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see SubmissionsActionEvent
	 */
	class SubmissionsActionListener implements ActionListener {
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {

			// String[] studentData = {ids[0], ids[1], ids[2]};
			ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTSUBMISSIONS, ids);
			routerData.getGuirouter().executeOrder(order);
		}
	}

	/**
	 * The listener interface for receiving active events.
	 * The class that is interested in processing a active
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addActiveListener<code> method. When
	 * the active event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ActiveEvent
	 */
	class ActiveListener implements ActionListener, DataSender, DataReceiver {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);
			SearchGUIOrder order = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(order);
			//
		}

		/* (non-Javadoc)
		 * @see control.dataSending.DataSender#send()
		 */
		@Override
		public DataFeildList send() {

			DataFeildList list = new DataFeildList();
			list.add(new DataFeild(AssignmentLabel.ID, ids[2]));
			list.add(new DataFeild(AssignmentLabel.COURSE_ID, ids[1]));

			return list;
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient message) {

			if (message == null) {
				System.out.println("error receiving the assignment label");
				return;
			}
			DataFeildList assignment = message.getList().get(0);
			assignment.delete(AssignmentLabel.ACTIVE);

			boolean isActive = active.isSelected();
			String active;
			if (isActive) {
				active = "t";
			} else {
				active = "f";
			}
			assignment.add(new DataFeild(AssignmentLabel.ACTIVE, active));

			// System.out.println("What is going on????");
			packager.clearReceivers();
			ModifyGUIOrder update = new ModifyGUIOrder(assignment, packager);
			routerData.getGuirouter().executeOrder(update);
		}

	}

	/**
	 * The listener interface for receiving returnAction events.
	 * The class that is interested in processing a returnAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addReturnActionListener<code> method. When
	 * the returnAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ReturnActionEvent
	 */
	class ReturnActionListener implements ActionListener {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] studentData = { ids[0], ids[1] };
			ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTS, studentData);
			routerData.getGuirouter().executeOrder(order);

		}
	}

}
