package view.professor;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.InsertGUIOrder;
import control.dataGUIOrders.SendAssignmentToServerGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import model.labels.CourseLabel;
import orders.guiOrder.ChangePanelOrder;
import orders.guiOrder.DisplayProfOrder;
import view.Field;
import view.RouterData;
import view.View;


/**
 * The Class AddAssignmentWindow.
 * @author Ashley
 */
public class AddAssignmentWindow implements DataSender, DataReceiver, ProfessorPanel {
	
	/** The window. */
	private JDialog window;
	
	/** The panel. */
	private JPanel panel;
	
	/** The choose file. */
	private JButton chooseFile;
	
	/** The selected file. */
	private File selectedFile;
	
	/** The due date. */
	private Field dueDate;
	
	/** The active. */
	private JCheckBox active;
	
	/** The upload. */
	private JButton upload;

	/** The router. */
	private RouterData router;
	
	/** The packager. */
	private ClientPackager packager;
	
	/** The ids. */
	private String[] ids;
	// ids[0] = profID
	// ids[1] = courseID

	/**
	 * Instantiates a new adds the assignment window.
	 *
	 * @param router the router
	 * @param p the p
	 * @param ids the ids
	 */
	public AddAssignmentWindow(RouterData router, ClientPackager p, String[] ids) {

		this.router = router;
		packager = p;
		packager.clearReceivers();
		packager.addReceiver(this);
		this.ids = ids;
		// System.out.println(profID);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

		window = new JDialog();
		window.setSize(screenSize.width / 2, screenSize.height / 2);
		window.setTitle("Upload an Assignment");

		panel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		panel.setBackground(View.WINDOW_BACKGROUND_COLOR);

		chooseFile = new JButton("Choose File");
		addChooseFileListener();

		dueDate = new Field("Due Date: ");

		active = new JCheckBox("Active");
		active.setBackground(View.WINDOW_BACKGROUND_COLOR);

		upload = new JButton("UPLOAD");
		upload.addActionListener(new UploadListener());

		//

		// panel.add(new Label("testing add assignment window"));
		panel.add(chooseFile, c);
		panel.add(dueDate.getLabel(), c);
		panel.add(dueDate.getText(), c);
		panel.add(active, c);
		panel.add(upload, c);

		window.add(panel);
		window.setVisible(true);
	}

	/* (non-Javadoc)
	 * @see view.professor.ProfessorPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		// TODO Auto-generated method stub
		ids = arguments;

	}

	/**
	 * Adds the choose file listener.
	 */
	public void addChooseFileListener() {
		chooseFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// open file explorer
				// TO DO: ADD ERROR CHECKING
				File file = new File("C:");
				JFileChooser fileBrowser = new JFileChooser();
				if (fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					selectedFile = fileBrowser.getSelectedFile();
				}

				// Setup if time - good file explorer
				// Desktop desktop = Desktop.getDesktop();
				// try {
				// desktop.open(file);
				// } catch (IOException e) {
				// System.out.println("error opening file explorer");
				// e.printStackTrace();
				// }

			}

		});
	}

	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	@Override
	public void receive(MessageForClient list) {
		// TODO Auto-generated method stub

	}

	/**
	 * Escape path.
	 *
	 * @param path the path
	 * @return the string
	 */
	public static String escapePath(String path) {
		return path.replace("\\", "\\\\");
	}

	/* (non-Javadoc)
	 * @see control.dataSending.DataSender#send()
	 */
	@Override
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();

		list.add(new DataFeild(AssignmentLabel.COURSE_ID, ids[1]));

//		File parent = selectedFile.getParentFile();
		String path = selectedFile.getAbsolutePath();
		path = escapePath(path);

		String[] nameParts = selectedFile.getName().split("\\.");
		String name = nameParts[0];
		String extension = "." + nameParts[1];

		list.add(new DataFeild(AssignmentLabel.PATH, path));
		list.add(new DataFeild(AssignmentLabel.TITLE, name + extension));
		list.add(new DataFeild(AssignmentLabel.EXTENSION, extension));

		String isActive;
		if (active.isSelected()) {
			isActive = "T";
		} else {
			isActive = "F";
		}
		list.add(new DataFeild(AssignmentLabel.ACTIVE, isActive));

		list.add(new DataFeild(AssignmentLabel.DUE_DATE, dueDate.getText().getText()));

		return list;
	}

	/**
	 * The listener interface for receiving upload events.
	 * The class that is interested in processing a upload
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addUploadListener<code> method. When
	 * the upload event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see UploadEvent
	 */
	class UploadListener implements ActionListener, DataReceiver, panelNames {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);

			SendAssignmentToServerGUIOrder order = new SendAssignmentToServerGUIOrder(send(), packager);
			router.getGuirouter().executeOrder(order);

			// try {
			// Thread.sleep(100);
			// } catch (InterruptedException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// JOptionPane.showMessageDialog(null, "New Assignment Uploaded", "Message",
			// JOptionPane.PLAIN_MESSAGE );
			// window.dispose();
			// ChangePanelOrder panel = new ChangePanelOrder(ASSIGNMENTS, ids);
			// router.getGuirouter().executeOrder(panel);
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient list) {
			JOptionPane.showMessageDialog(null, "New Assignment Uploaded", "Message", JOptionPane.PLAIN_MESSAGE);
			window.dispose();
			// DisplayProfOrder order = new DisplayProfOrder();
			ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTS, ids);
			router.getGuirouter().executeOrder(order);

		}

	}

}
