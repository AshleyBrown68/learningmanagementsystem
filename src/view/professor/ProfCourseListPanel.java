package view.professor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.GUIRouter;
import control.clientDataReceiving.MessageForClient;
import control.clientDataReceiving.Router;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
import model.labels.UserLabel;
import orders.databaseOrders.SearchOrder;
import orders.guiOrder.ChangePanelOrder;
import view.ListDataElement;
import view.ListDataElementAssignment;
import view.ListDataElementCourse;
import view.RouterData;
import view.View;
import view.ResultsList;
//import view.student.StudentCourseListPanel;

public class ProfCourseListPanel extends JPanel implements DataSender, DataReceiver, ProfessorPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8518567933298029321L;

	// private JCheckBox active;
	protected ResultsList<ListDataElementCourse> courses;
	private JButton newCourse;
	private JButton refresh;
	private JPanel south;

	// private GUIRouter guirouter;
	// private Router router;

	private RouterData routerData;
	private ClientPackager packager;
	private String profID;

	public ProfCourseListPanel(RouterData data, ClientPackager p) {
		this.setBackground(View.BACKGROUND_COLOUR);
		// this.guirouter = guirouter;
		// this.router = router;

		routerData = data;
		packager = p;

		this.setLayout(new BorderLayout());

		// active = new JCheckBox("Active ");
		courses = new ResultsList<ListDataElementCourse>();
		courses.addListSelectionListener(new ListActionListener());

		newCourse = new JButton("Add New Course");
		newCourse.addActionListener(new NewCourseListener());

		refresh = new JButton("Refresh");
		refresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String[] data = { profID };
				setup(data);
			}
		});

		south = new JPanel();
		// south.setBackground(new Color(132, 173, 244));
		south.setBackground(View.BACKGROUND_COLOUR);

		add(new Label("Welcome to sketchy, offbrand D2L!"), BorderLayout.NORTH);

		this.add(courses, BorderLayout.CENTER);

		south.add(newCourse);
		south.add(refresh);
		add(south, BorderLayout.SOUTH);

		// setup();

	}

	// needs to run when course list becomes the visible panel
	// data[0] = profID
	public void setup(String[] data) {
		courses.clear();
		// System.out.println("Setup run");
		this.profID = data[0];
		// System.out.println(profID);

		// search - get an (Array?) of ListDataElementCourse
		packager.clearReceivers();
		packager.addReceiver(this);
		SearchGUIOrder search = new SearchGUIOrder(send(), packager);
		routerData.getGuirouter().executeOrder(search);

		//

	}

	class ListActionListener implements ListSelectionListener, panelNames {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			int index = courses.getListOfResults().getSelectedIndex();
			if (index != -1) {
				ListDataElementCourse selectedCourse = (ListDataElementCourse) courses.getListModel()
						.getElementAt(index);
				String courseID = selectedCourse.getID();

				String[] setup = { profID, courseID };
				courses.clear();
				ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTS, setup);
				routerData.getGuirouter().executeOrder(order);
			}

		}

		// @Override
		// public DataFeildList send() {
		// DataFeildList list=new DataFeildList();
		//
		////
		//// list.add(new DataFeild(UserLabel.EMAIL, username.getText().getText()));
		//
		// list.add(new DataFeild(CourseLabel.PROF_ID, profID));
		// return list;
		// }

	}

	class NewCourseListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			new AddCourseWindow(routerData, packager, profID);
		}

	}

	public ResultsList<ListDataElementCourse> getCourseList() {
		return courses;
	}

	@Override
	public void receive(MessageForClient message) {
		ArrayList<DataFeildList> list = message.getList();
		for (DataFeildList element : list) {
			ListDataElementCourse courseFound = new ListDataElementCourse(element);
			courses.addEntry(courseFound);
		}

	}

	// creates list for opening assignmenListPanel
	@Override
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();

		//
		// list.add(new DataFeild(UserLabel.EMAIL, username.getText().getText()));

		list.add(new DataFeild(CourseLabel.PROF_ID, profID));
		return list;
	}

}
