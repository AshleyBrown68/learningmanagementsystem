package view.professor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.InsertGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
import orders.databaseOrders.InsertOrder;
import orders.guiOrder.DisplayProfOrder;
import view.Field;
import view.RouterData;
import view.View;

/**
 * The Class AddCourseWindow.
 * @author Ashley
 */
public class AddCourseWindow implements DataSender, DataReceiver, ProfessorPanel {

	/** The window. */
	private JDialog window;
	
	/** The panel. */
	private JPanel panel;
	
	/** The course name. */
	// private Field courseID;
	private Field courseName;
	
	/** The active. */
	private JCheckBox active;
	
	/** The add. */
	private JButton add;
	
	/** The gbc. */
	private GridBagConstraints gbc;

	/** The router. */
	private RouterData router;
	
	/** The packager. */
	private ClientPackager packager;
	
	/** The prof ID. */
	private String profID;

	/**
	 * Instantiates a new adds the course window.
	 *
	 * @param router the router
	 * @param p the p
	 * @param profID the prof ID
	 */
	public AddCourseWindow(RouterData router, ClientPackager p, String profID) {

		this.router = router;
		packager = p;
		packager.clearReceivers();
		packager.addReceiver(this);
		this.profID = profID;
		// System.out.println(profID);

		window = new JDialog();
		window.setSize(400, 200);
		panel = new JPanel(new GridBagLayout());
		panel.setBackground(View.WINDOW_BACKGROUND_COLOR);
		// courseID = new Field(panel, 0, "Course ID: ");
		courseName = new Field(panel, 1, "Course Name: ");
		active = new JCheckBox("Active");
		active.setBackground(View.WINDOW_BACKGROUND_COLOR);

		add = new JButton("Add");
		add.addActionListener(new AddListener());

		addComponents();

		window.add(panel);
		window.setVisible(true);
	}

	/**
	 * Adds the components.
	 */
	private void addComponents() {
		gbc = new GridBagConstraints();
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;

		gbc.gridy = 1;
		gbc.gridx = 3;
		panel.add(active, gbc);

		gbc.gridy = 2;
		// gbc.gridwidth = 3;
		gbc.gridx = 1;
		panel.add(add, gbc);
	}

	/**
	 * The listener interface for receiving add events.
	 * The class that is interested in processing a add
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addAddListener<code> method. When
	 * the add event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see AddEvent
	 */
	class AddListener implements ActionListener {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			InsertGUIOrder order = new InsertGUIOrder(send(), packager);
			router.getGuirouter().executeOrder(order);

		}

	}

	/* (non-Javadoc)
	 * @see control.dataSending.DataSender#send()
	 */
	@Override
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();

		// list.add(new DataFeild(CourseLabel.ID, courseID.getText().getText()));

		list.add(new DataFeild(CourseLabel.PROF_ID, profID));
		list.add(new DataFeild(CourseLabel.NAME, courseName.getText().getText()));

		String isActive;
		if (active.isSelected()) {
			isActive = "T";
		} else {
			isActive = "F";
		}
		list.add(new DataFeild(CourseLabel.ACTIVE, isActive));

		return list;
	}

	/* (non-Javadoc)
	 * @see view.professor.ProfessorPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	@Override
	public void receive(MessageForClient list) {
		JOptionPane.showMessageDialog(null, "New Course Inserted", "Message", JOptionPane.PLAIN_MESSAGE);
		window.dispose();
		DisplayProfOrder order = new DisplayProfOrder(profID);
		router.getGuirouter().executeOrder(order);

	}
}
