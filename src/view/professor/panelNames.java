package view.professor;


/**
 * The Interface with all of the panel names.
 * @author Ashley
 * 
 */
public interface panelNames {

	/** The Constant ASSIGNMENTS. */
	public static final String ASSIGNMENTS = "assignments";
	
	/** The Constant STUDENTS. */
	public static final String STUDENTS = "students";
	
	/** The Constant COURSES. */
	public static final String COURSES = "courses";
	
	/** The Constant ASSIGNMENTSUBMISSIONS. */
	public static final String ASSIGNMENTSUBMISSIONS = "assignmentSubmissions";
	
	/** The Constant SUBMISSION. */
	public static final String SUBMISSION = "submission";
	
	/** The Constant GRADES. */
	public static final String GRADES = "grades";
	
	/** The Constant DETAILS. */
	public static final String DETAILS = "details";
	
	/** The Constant CHATROOM. */
	public static final String CHATROOM = "chatroom";

	/** The current. */
	public String CURRENT = "";

}
