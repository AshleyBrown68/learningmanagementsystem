package view.professor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.DeleteGUIOrder;
import control.dataGUIOrders.InsertGUIOrder;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
import model.labels.StudentEnrollmentLabel;
import model.labels.SubmissionLabel;
import model.labels.UserLabel;
import orders.guiOrder.ChangePanelOrder;
//import view.Field;
import view.ListDataElement;
import view.ListDataElementCourse;
import view.ListDataElementStudent;
import view.RouterData;
import view.View;
import view.ResultsList;


/**
 * The Class StudentListPanel.
 * Holds a list of students for the professor to peruse.
 * @author Ashley
 */
public class StudentListPanel extends JPanel implements ProfessorPanel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The layout. */
	private BorderLayout layout;

	/** The student search results. */
	private ResultsList<ListDataElement> studentSearchResults;

	/** The search term. */
	private JTextField searchTerm;

	/** The button to start the search. */
	private JButton search;

	/** The button to return to assignments. */
	private JButton returnToAssignments;

	/** The button to show all students enrolled in the course. */
	private JButton showAll;

	// Add a JButton to show all the students, enrolled or not?

	/** The router data. */
	private RouterData routerData;

	/** The packager. */
	private ClientPackager packager;

	/**
	 * The ids that specify the page currently displayed.
	 * 
	 * ids[0] = profID ids[1] = courseID
	 */
	private String[] ids;

	/**
	 * Instantiates a new student list panel.
	 *
	 * @param data
	 *            the data
	 * @param p
	 *            the packager
	 */
	public StudentListPanel(RouterData data, ClientPackager p) {
		this.setBackground(View.BACKGROUND_COLOUR);
		layout = new BorderLayout();
		this.setLayout(layout);
		routerData = data;
		packager = p;

		studentSearchResults = new ResultsList<ListDataElement>();
		studentSearchResults.addListSelectionListener(new StudentListListener());

		searchTerm = new JTextField();
		searchTerm.setColumns(15);

		search = new JButton("Search by Last Name");
		search.addActionListener(new SearchListener());

		showAll = new JButton("Show all students enrolled");
		showAll.addActionListener(new ShowAllListener());

		returnToAssignments = new JButton("Return to Course Home");
		returnToAssignments.addActionListener(new ReturnActionListener());
		// remove = new JButton("Remove Student");
		// add = new JButton("Add Student");

		add(studentSearchResults, layout.CENTER);

		JPanel north = new JPanel();
		north.setBackground(View.BACKGROUND_COLOUR);
		north.add(searchTerm);
		north.add(search);
		north.add(showAll);
		add(north, layout.NORTH);

		add(returnToAssignments, layout.SOUTH);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see view.professor.ProfessorPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		ids = arguments;

		studentSearchResults.clear();

	}

	/**
	 * The listener interface for receiving studentList events. The class that is
	 * interested in processing a studentList event implements this interface, and
	 * the object created with that class is registered with a component using the
	 * component's <code>addStudentListListener<code> method. When the studentList
	 * event occurs, that object's appropriate method is invoked.
	 *
	 * @see StudentListEvent
	 */
	class StudentListListener implements ListSelectionListener, DataReceiver {

		/** The student ID. */
		String studentID;

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.
		 * ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent arg0) {
			int index = studentSearchResults.getListOfResults().getSelectedIndex();
			if (index != -1) {
				ListDataElementStudent student = (ListDataElementStudent) studentSearchResults.getListModel()
						.getElementAt(index);
				String type = student.getType();
				packager.clearReceivers();
				// packager.addReceiver(this);

				if (type.equals("ID")) {
					JFrame frame = new JFrame();
					int result;
					result = JOptionPane.showConfirmDialog(frame, "Do you want to remove this student from the class?",
							"Change Enrollement", JOptionPane.YES_NO_OPTION);

					if (result == JOptionPane.YES_OPTION) {
						DataFeildList list = new DataFeildList();
						list.add(new DataFeild(StudentEnrollmentLabel.ID, student.getID()));

						DeleteGUIOrder delete = new DeleteGUIOrder(list, packager);
						routerData.getGuirouter().executeOrder(delete);
						//
					}
				} else if (type.equals("NAME")) {
					packager.addReceiver(this);

					DataFeildList list = new DataFeildList();
					list.add(new DataFeild(StudentEnrollmentLabel.STUDENT_ID, student.getID()));
					studentID = student.getID();
					list.add(new DataFeild(StudentEnrollmentLabel.COURSE_ID, ids[1]));

					SearchGUIOrder search = new SearchGUIOrder(list, packager);
					routerData.getGuirouter().executeOrder(search);

				}

				// String id = student.getID();
				// DataFeildList list = new DataFeildList();
				// list.add(new DataFeild(UserLabel.ID, id));
				//
				// SearchGUIOrder search = new SearchGUIOrder(list, packager);
				// routerData.getGuirouter().executeOrder(search);
			}

		}

		/**
		 * Receives student enrollment data which it uses to determine whether to offer
		 * to enroll or remove the student. Executes either insertion or deletion if
		 * confirmed.
		 *
		 * @param enrollment the enrollment
		 */
		@Override
		public void receive(MessageForClient enrollment) {
			// receives list of student enrollment labels

			for (DataFeildList x : enrollment.getList()) {
				System.out.println(x);
			}

			if (enrollment.getList().isEmpty()) {
				JFrame frame = new JFrame();
				int result;
				result = JOptionPane.showConfirmDialog(frame, "Do you want to enroll this student in the class?",
						"Change Enrollement", JOptionPane.YES_NO_OPTION);

				if (result == JOptionPane.YES_OPTION) {
					DataFeildList list = new DataFeildList();
					list.add(new DataFeild(StudentEnrollmentLabel.STUDENT_ID, studentID));
					list.add(new DataFeild(StudentEnrollmentLabel.COURSE_ID, ids[1]));

					InsertGUIOrder insert = new InsertGUIOrder(list, packager);

					// DeleteGUIOrder delete = new DeleteGUIOrder(list, packager);
					routerData.getGuirouter().executeOrder(insert);
					// add student and class to class enrollment (insert)
				}
			} else {
				JFrame frame = new JFrame();
				int result;
				result = JOptionPane.showConfirmDialog(frame, "Do you want to remove this student from the class?",
						"Change Enrollement", JOptionPane.YES_NO_OPTION);

				if (result == JOptionPane.YES_OPTION) {
					DataFeildList list = new DataFeildList();
					list.add(new DataFeild(StudentEnrollmentLabel.ID,
							enrollment.getList().get(0).get(StudentEnrollmentLabel.ID).getData()));

					DeleteGUIOrder delete = new DeleteGUIOrder(list, packager);
					routerData.getGuirouter().executeOrder(delete);
					//
				}

			}

		}

	}

	// Getters

	/**
	 * Gets the student search results.
	 *
	 * @return the student search results
	 */
	public ResultsList<ListDataElement> getStudentSearchResults() {
		return studentSearchResults;
	}

	/**
	 * Gets the search term.
	 *
	 * @return the search term
	 */
	public JTextField getSearchTerm() {
		return searchTerm;
	}

	/**
	 * Gets the search.
	 *
	 * @return the search
	 */
	public JButton getSearch() {
		return search;
	}

	/**
	 * The listener interface for receiving search events. The class that is
	 * interested in processing a search event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addSearchListener<code> method. When the search event
	 * occurs, that object's appropriate method is invoked.
	 *
	 * @see SearchEvent
	 */
	class SearchListener implements ActionListener, DataSender, DataReceiver {

		/**
		 * Searches the Database by user's last name.
		 *
		 * @param arg0 the arg 0
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);
			SearchGUIOrder search = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(search);

		}

		/**
		 * Creates a list with the search term (a last name).
		 *
		 * @return the data feild list
		 */
		@Override
		public DataFeildList send() {
			DataFeildList list = new DataFeildList();
			list.add(new DataFeild(UserLabel.LASTNAME, searchTerm.getText()));
			return list;
		}

		/**
		 * Receives a list of students that it displays on the list.
		 * 
		 * @param message
		 *            the results of the search
		 * 
		 * 
		 */
		@Override
		public void receive(MessageForClient message) {
			studentSearchResults.clear();

			ArrayList<DataFeildList> list = message.getList();
			if (list.isEmpty()) {
				JOptionPane.showMessageDialog(null, "No Results Found", "Message", JOptionPane.PLAIN_MESSAGE);

			}
			for (DataFeildList element : list) {
				String userType = element.get(UserLabel.TYPE).getData();
				if(userType.equalsIgnoreCase("S")){
					ListDataElementStudent courseFound = new ListDataElementStudent(element);
					studentSearchResults.addEntry(courseFound);
				}
				
				
			}

		}
	}

	/**
	 * The listener interface for receiving showAll events. The class that is
	 * interested in processing a showAll event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addShowAllListener<code> method. When the showAll event
	 * occurs, that object's appropriate method is invoked.
	 *
	 * @see ShowAllEvent
	 */
	class ShowAllListener implements ActionListener, DataSender, DataReceiver {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);
			SearchGUIOrder search = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(search);

		}

		/**
		 * Creates a list with the course ID.
		 *
		 * @return the data feild list
		 */
		@Override
		public DataFeildList send() {
			DataFeildList list = new DataFeildList();
			// list.add(new DataFeild(StudentEnrollmentLabel.STUDENT_ID,
			// searchTerm.getText()));
			list.add(new DataFeild(StudentEnrollmentLabel.COURSE_ID, ids[1]));
			return list;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.
		 * MessageForClient)
		 */
		@Override
		public void receive(MessageForClient message) {
			studentSearchResults.clear();

			ArrayList<DataFeildList> list = message.getList();
			if (list.isEmpty()) {
				JOptionPane.showMessageDialog(null, "No Results Found", "Message", JOptionPane.PLAIN_MESSAGE);

			}
			for (DataFeildList element : list) {
				ListDataElementStudent courseFound = new ListDataElementStudent(element);
				studentSearchResults.addEntry(courseFound);
			}

		}

	}

	/**
	 * The listener interface for receiving returnAction events. The class that is
	 * interested in processing a returnAction event implements this interface, and
	 * the object created with that class is registered with a component using the
	 * component's <code>addReturnActionListener<code> method. When the returnAction
	 * event occurs, that object's appropriate method is invoked.
	 *
	 * @see ReturnActionEvent
	 */
	class ReturnActionListener implements ActionListener, panelNames {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		// Opens assignmentListPanel
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] courseData = { ids[0], ids[1] };
			ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTS, courseData);
			routerData.getGuirouter().executeOrder(order);

		}

	}

}
