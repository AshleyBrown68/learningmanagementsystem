package view;

import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;

// TODO: Auto-generated Javadoc
//import data.dataFeild.DataFeildList;

/**
 * The Class ListDataElementAssignment.
 * @author Ashley
 */
public class ListDataElementAssignment extends ListDataElement{

	/**
	 * Instantiates a new list data element assignment.
	 *
	 * @param data the data
	 */
	public ListDataElementAssignment(DataFeildList data) {
//need a assignment label
		AssignmentLabel assignmentLabelId = AssignmentLabel.ID;
		id = data.get(assignmentLabelId).getData();
		
		AssignmentLabel name = AssignmentLabel.TITLE;
		display = data.get(name).getData();
	}


}
