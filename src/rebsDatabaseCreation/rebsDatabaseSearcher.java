package rebsDatabaseCreation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.*;

//import data.dataFeild.DataFeild;
//import data.dataFeild.DataFeildList;
//import data.labels.*;

public class rebsDatabaseSearcher {

	/** The jdbc connection. */
	public Connection jdbc_connection;

	/** The statement. */
	public PreparedStatement statement;

	public rebsDatabaseSearcher() {
		// TODO Auto-generated constructor stub
	}

	public ArrayList<DataFeildList> searchforStringUserTable(String keyToSearch, String myUserLabelName) {
		String sql = "SELECT * FROM " + "UserTable" + " WHERE " + myUserLabelName + " = ?";
		// String sql = "SELECT * FROM " + "UserTable" + " WHERE "+
		// data.get(myUserLabel).getData() + " = ?";
		ResultSet myClient;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setString(1, keyToSearch);
			myClient = statement.executeQuery();
			ArrayList<DataFeildList> tableMatch = new ArrayList<DataFeildList>();

			while (myClient.next()) {
				// CourseLabel courseLabel_id = CourseLabel.ID;
				// String = data.get(courseLabel_id).getData();
				DataFeildList dList = new DataFeildList();
				DataFeild myID = new DataFeild(UserLabel.ID, myClient.getString("ID"));
				dList.add(myID);
				DataFeild myPASSWORD = new DataFeild(UserLabel.PASSWORD, myClient.getString("PASSWORD"));
				dList.add(myPASSWORD);
				DataFeild myEMAIL = new DataFeild(UserLabel.EMAIL, myClient.getString("EMAIL"));
				dList.add(myEMAIL);
				DataFeild myFIRSTNAME = new DataFeild(UserLabel.FIRSTNAME, myClient.getString("FIRSTNAME"));
				dList.add(myFIRSTNAME);
				DataFeild myLASTNAME = new DataFeild(UserLabel.LASTNAME, myClient.getString("LASTNAME"));
				dList.add(myLASTNAME);
				DataFeild myTYPE = new DataFeild(UserLabel.TYPE, myClient.getString("TYPE"));
				dList.add(myTYPE);

				tableMatch.add(dList);
			}

			return tableMatch;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<DataFeildList> searchforLabelCourseTable(String keyToSearch, String myUserLabelName) {
		String sql = "SELECT * FROM " + "CourseTable" + " WHERE " + myUserLabelName + " = ?";
		// String sql = "SELECT * FROM " + "UserTable" + " WHERE "+
		// data.get(myUserLabel).getData() + " = ?";
		ResultSet myClient;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setString(1, keyToSearch);
			myClient = statement.executeQuery();
			ArrayList<DataFeildList> tableMatch = new ArrayList<DataFeildList>();

			while (myClient.next()) {
				DataFeildList dList = new DataFeildList();
				DataFeild data1 = new DataFeild(CourseLabel.ID, myClient.getString("ID"));
				dList.add(data1);
				DataFeild data2 = new DataFeild(CourseLabel.PROF_ID, myClient.getString("PROF_ID"));
				dList.add(data2);
				DataFeild data3 = new DataFeild(CourseLabel.NAME, myClient.getString("NAME"));
				dList.add(data3);
				DataFeild data4 = new DataFeild(CourseLabel.ACTIVE, myClient.getString("ACTIVE"));
				dList.add(data4);

				tableMatch.add(dList);
			}

			return tableMatch;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<DataFeildList> searchforLabelStudentEnrollmentTable(String keyToSearch, String myUserLabelName) {
		String sql = "SELECT * FROM " + "StudentEnrollmentTable" + " WHERE " + myUserLabelName + " = ?";
		// String sql = "SELECT * FROM " + "UserTable" + " WHERE "+
		// data.get(myUserLabel).getData() + " = ?";
		ResultSet myClient;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setString(1, keyToSearch);
			myClient = statement.executeQuery();
			ArrayList<DataFeildList> tableMatch = new ArrayList<DataFeildList>();

			while (myClient.next()) {
				DataFeildList dList = new DataFeildList();
				DataFeild data1 = new DataFeild(StudentEnrollmentLabel.ID, myClient.getString("ID"));
				dList.add(data1);
				DataFeild data2 = new DataFeild(StudentEnrollmentLabel.STUDENT_ID, myClient.getString("STUDENT_ID"));
				dList.add(data2);
				DataFeild data3 = new DataFeild(StudentEnrollmentLabel.COURSE_ID, myClient.getString("COURSE_ID"));
				dList.add(data3);

				tableMatch.add(dList);
			}

			return tableMatch;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<DataFeildList> searchforLabelAssignmentTable(String keyToSearch, String myUserLabelName) {
		String sql = "SELECT * FROM " + "AssignmentTable" + " WHERE " + myUserLabelName + " = ?";
		// String sql = "SELECT * FROM " + "UserTable" + " WHERE "+
		// data.get(myUserLabel).getData() + " = ?";
		ResultSet myClient;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setString(1, keyToSearch);
			myClient = statement.executeQuery();
			ArrayList<DataFeildList> tableMatch = new ArrayList<DataFeildList>();

			while (myClient.next()) {
				DataFeildList dList = new DataFeildList();
				DataFeild data1 = new DataFeild(AssignmentLabel.ID, myClient.getString("ID"));
				dList.add(data1);
				DataFeild data2 = new DataFeild(AssignmentLabel.COURSE_ID, myClient.getString("COURSE_ID"));
				dList.add(data2);
				DataFeild data3 = new DataFeild(AssignmentLabel.TITLE, myClient.getString("TITLE"));
				dList.add(data3);
				DataFeild data4 = new DataFeild(AssignmentLabel.PATH, myClient.getString("PATH"));
				dList.add(data4);
				DataFeild data5 = new DataFeild(AssignmentLabel.ACTIVE, myClient.getString("ACTIVE"));
				dList.add(data5);
				DataFeild data6 = new DataFeild(AssignmentLabel.DUE_DATE, myClient.getString("DUE_DATE"));
				dList.add(data6);

				tableMatch.add(dList);
			}

			return tableMatch;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<DataFeildList> searchforLabelSubmissionTable(String keyToSearch, String myUserLabelName) {
		String sql = "SELECT * FROM " + "SubmissionTable" + " WHERE " + myUserLabelName + " = ?";
		// String sql = "SELECT * FROM " + "UserTable" + " WHERE "+
		// data.get(myUserLabel).getData() + " = ?";
		ResultSet myClient;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setString(1, keyToSearch);
			myClient = statement.executeQuery();
			ArrayList<DataFeildList> tableMatch = new ArrayList<DataFeildList>();

			while (myClient.next()) {
				DataFeildList dList = new DataFeildList();
				DataFeild data1 = new DataFeild(SubmissionLabel.ID, myClient.getString("ID"));
				dList.add(data1);
				DataFeild data2 = new DataFeild(SubmissionLabel.ASSIGNMENT_ID, myClient.getString("ASSIGN_ID"));
				dList.add(data2);
				DataFeild data3 = new DataFeild(SubmissionLabel.STUDENT_ID, myClient.getString("STUDENT_ID"));
				dList.add(data3);
				DataFeild data4 = new DataFeild(SubmissionLabel.PATH, myClient.getString("PATH"));
				dList.add(data4);
				DataFeild data5 = new DataFeild(SubmissionLabel.TITLE, myClient.getString("TITLE"));
				dList.add(data5);
				DataFeild data6 = new DataFeild(SubmissionLabel.SUBMISSION_GRADE,
						myClient.getString("SUBMISSION_GRADE"));
				dList.add(data6);
				DataFeild data7 = new DataFeild(SubmissionLabel.COMMENTS, myClient.getString("COMMENTS"));
				dList.add(data7);
				DataFeild data8 = new DataFeild(SubmissionLabel.TIMESTAMP, myClient.getString("TIMESTAMP"));
				dList.add(data8);

				tableMatch.add(dList);
			}

			return tableMatch;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<DataFeildList> searchforLabelGradeTable(String keyToSearch, String myUserLabelName) {
		String sql = "SELECT * FROM " + "GradeTable" + " WHERE " + myUserLabelName + " = ?";
		// String sql = "SELECT * FROM " + "UserTable" + " WHERE "+
		// data.get(myUserLabel).getData() + " = ?";
		ResultSet myClient;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setString(1, keyToSearch);
			myClient = statement.executeQuery();
			ArrayList<DataFeildList> tableMatch = new ArrayList<DataFeildList>();

			while (myClient.next()) {
				DataFeildList dList = new DataFeildList();
				DataFeild data1 = new DataFeild(GradeLabel.ID, myClient.getString("ID"));
				dList.add(data1);
				DataFeild data2 = new DataFeild(GradeLabel.ASSIGNMENT_ID, myClient.getString("ASSIGN_ID"));
				dList.add(data2);
				DataFeild data3 = new DataFeild(GradeLabel.STUDENT_ID, myClient.getString("STUDENT_ID"));
				dList.add(data3);
				DataFeild data4 = new DataFeild(GradeLabel.COURSE_ID, myClient.getString("COURSE_ID"));
				dList.add(data4);
				DataFeild data5 = new DataFeild(GradeLabel.ASSIGNMENT_GRADE, myClient.getString("ASSIGNMENT_GRADE"));
				dList.add(data5);

				tableMatch.add(dList);
			}

			return tableMatch;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
