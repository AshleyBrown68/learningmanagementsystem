package ServerOrder;

import java.util.ArrayList;

import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
//import model.labels.StudentEnrollmentLabel;
import model.labels.UserLabel;
import orders.databaseOrders.SearchOrder;
import rebsEmailStuff.rebsEmailHolder;

public class EmailRequestFromStudentGUItoServer extends DatabaseServerOrder {

	private static final long serialVersionUID = 6209662047768125022L;
	private String title;
	private String message;

	public EmailRequestFromStudentGUItoServer(SearchOrder serverOrder, String title, String message) {
		super(serverOrder);
		this.title = title;
		this.message = message;
	}

	public void execute() {
		super.execute(); // super's Dataorder dataOrder should now be executed.
							// it should contain the ArrayList<DataFeildList> containing the prof's ID
							// enrolled in the course with the inputted course_id.

		ArrayList<DataFeildList> myListBeforeForLoop = super.forClient.getMessage().getList();
		System.out.println(myListBeforeForLoop);
		System.out.println("Above should be myListBeforeForLoop");
		ArrayList<String> theProfEmails = new ArrayList<String>();
		// theProfEmails.add(super.forClient.getMessage().getList().get(0).get(CourseLabel.PROF_ID).getData());

		// for (DataFeildList myCourseDataFromSearch : myListBeforeForLoop) {
		// // myCourseData should hold a single prof's ID. (with other id's)
		//
		SearchOrder mySearchOrder = new SearchOrder();
		DataFeildList myUserTableDataforSecondSearch = new DataFeildList();
		myUserTableDataforSecondSearch
				.add(new DataFeild(UserLabel.ID, myListBeforeForLoop.get(0).get(CourseLabel.PROF_ID).getData()));
		mySearchOrder.setData(myUserTableDataforSecondSearch);

		//
		// // String message = mySearchOrder.getName() + " completed sucessfully";
		ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
		list.add(mySearchOrder.getList());
		try {
			list = super.getBox().getRouter().executeOrder(mySearchOrder);
		} catch (IllegalArgumentException e) {
			// when unsuccessful, the message is that it was not successful
			message = e.getMessage();
			e.printStackTrace();
		}
		theProfEmails.add(list.get(0).get(UserLabel.EMAIL).getData());
		System.out.println(theProfEmails);
		System.out.println("above should be the prof's email?");
		// } // END OF FOR
		// allStudentEmails should contain all student emails at this point
		rebsEmailHolder myrebsEmailHolder = new rebsEmailHolder(theProfEmails, title, message);
		System.out.println("After trying to send email");

	}
}
