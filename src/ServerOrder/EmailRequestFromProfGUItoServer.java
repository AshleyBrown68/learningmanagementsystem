package ServerOrder;

import java.util.ArrayList;

import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.StudentEnrollmentLabel;
import model.labels.UserLabel;
import orders.databaseOrders.SearchOrder;
import rebsEmailStuff.rebsEmailHolder;
/**
 * this class is an order which the server executes when a professer wants to email
 * everybody in the class. It does what needs to be done to email everybody
 * @author brere
 *
 */
public class EmailRequestFromProfGUItoServer extends DatabaseServerOrder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2615789162269527639L;
	private String title;
	private String message;
	public EmailRequestFromProfGUItoServer(SearchOrder serverOrder,String title,String message) {
		super(serverOrder);
		this.title=title;
		this.message=message; 
		//TODO NEED TO FIND WAY TO SET SUBJECT AND TEXT
	}

	public void execute() {
		super.execute(); // super's Dataorder dataOrder should now be executed.
							// it should contain the ArrayList<DataFeildList> containing all students' ID
							// enrolled in the course with the inputted course_id.
		
		ArrayList<DataFeildList> myListBeforeForLoop = super.forClient.getMessage().getList();
		System.out.println(myListBeforeForLoop);
		System.out.println("Above should be myListBeforeForLoop");
		ArrayList<String> allStudentEmails = new ArrayList<String>();
		
		for (DataFeildList myStudentEnrollmentDataFromSearch : myListBeforeForLoop) {
			// myStudentEnrollmentData should hold a single student's ID. (with other id's)

			SearchOrder mySearchOrder = new SearchOrder();
			DataFeildList myUserTableDataforSecondSearch = new DataFeildList();
			myUserTableDataforSecondSearch.add(new DataFeild(UserLabel.ID,
					myStudentEnrollmentDataFromSearch.get(StudentEnrollmentLabel.STUDENT_ID).getData()));
			mySearchOrder.setData(myUserTableDataforSecondSearch);
			// NEED to only set to search student_id!!!

			//String message = mySearchOrder.getName() + " completed sucessfully";
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(mySearchOrder.getList());
			try {
				list = super.getBox().getRouter().executeOrder(mySearchOrder);
			} catch (IllegalArgumentException e) {
				// when unsuccessful, the message is that it was not successful
				//message = e.getMessage();
				e.printStackTrace();
			}
			allStudentEmails.add(list.get(0).get(UserLabel.EMAIL).getData());
			
		}//END OF FOR
		//allStudentEmails should contain all student emails at this point
		rebsEmailHolder myrebsEmailHolder = new rebsEmailHolder(allStudentEmails, title, message);
		System.out.println("After trying to send email");

	}

}
