package ServerOrder;

import java.nio.file.Path;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import model.serverStuff.serverCommunication.ReturnOrder;
import model.serverStuff.serverCommunication.ReturnOrderForFileDowlnoadFromServer;
import orders.databaseOrders.SearchOrder;

public class GUIRequestForReceivingAssignmentFromServer extends DatabaseServerOrder {

	private static final long serialVersionUID = 1L;
	byte[] content;
	String myTextPath;
	Path myPath;
	String theClientDesieredPath;

	public GUIRequestForReceivingAssignmentFromServer(SearchOrder serverOrder) {
		super(serverOrder);
	}

	public ArrayList<DataFeildList> theExecuteFromDataBaseServerOrderThatReturnsList() {
		// when sucessful, the message is that the order completed successfully
		String message = dataOrder.getName() + " to search for file completed sucessfully";
		theClientDesieredPath = dataOrder.getList().get(AssignmentLabel.PATH).getData();
		dataOrder.getList().delete(AssignmentLabel.PATH);

		// when unsucessful, list has one element, being the original datafeilds from
		// the dataOrder
		ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
		list.add(dataOrder.getList());
		try {
			list = super.getBox().getRouter().executeOrder(dataOrder);
		} catch (IllegalArgumentException e) {
			// when unsuccessful, the message is that it was not successful
			message = e.getMessage();
		}

		// forClient.getMessage().setList(list);
		// forClient.getMessage().setMessage(message);
		//
		// super.getBox().getSender().send(forClient);

		return list;
	}

	public static String escapePath(String path) {
		return path.replace(" ", "\\");
	}

	@Override
	public void execute() {
		// super.execute();

		ArrayList<DataFeildList> mySearchingForFilePathFromServer = theExecuteFromDataBaseServerOrderThatReturnsList();

		// myTextPath =
		// super.getDatabaseOrder().getList().get(SubmissionLabel.PATH).getData();
		myTextPath = mySearchingForFilePathFromServer.get(0).get(AssignmentLabel.PATH).getData();
		myTextPath = escapePath(myTextPath);
		myPath = Paths.get(myTextPath);
		// the path here should be the path on the 'server side'. execute should still
		// be on the server.

		///////////////////////////////////// getting byte array from server

		File selectedFile = new File(myTextPath); // Add your file myTextPath here

		long length = selectedFile.length();
		content = new byte[(int) length]; // Converting Long to Int
		ReturnOrder oldReturn = forClient;

		try {
			FileInputStream fis = new FileInputStream(selectedFile);
			BufferedInputStream bos = new BufferedInputStream(fis);
			bos.read(content, 0, (int) length);

			bos.close(); // content is ready to send here

			forClient = new ReturnOrderForAssignmentDownloadFromServer(oldReturn.getMessage(), oldReturn.getDistributeID(),
					content);
			// super.getBox().getRouter().executeOrder(forClient);
			myTextPath = null;
			myPath = null;

			mySearchingForFilePathFromServer.get(0).delete(AssignmentLabel.PATH);
			System.out.println(theClientDesieredPath);
			mySearchingForFilePathFromServer.get(0).add(new DataFeild(AssignmentLabel.PATH, theClientDesieredPath));

			forClient.getMessage().setList(mySearchingForFilePathFromServer);
			String message = dataOrder.getName() + " to download completed sucessfully";

			forClient.getMessage().setMessage(message);

		} catch (FileNotFoundException e) {
			forClient.getMessage().setMessage("We were unable to find your file.  please try again later");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			forClient.getMessage().setMessage("unknown file error, please contaact technical suppeort");
		}
		super.getBox().getSender().send(forClient);
		/////////////////////////////////////////////////

		// // File newFile = new File(STORAGE_PATH + FILE_NAME + FILE_EXTENSION);
		// File newFile = new File(System.getProperty("user.home") + "/Documents/" +
		// myPath.getFileName());
		// // File newFile = new File(myTextPath);
		// try {
		// if (!newFile.exists())
		// newFile.createNewFile();
		// FileOutputStream writer = new FileOutputStream(newFile);
		// BufferedOutputStream bos = new BufferedOutputStream(writer);
		// bos.write(content);
		// bos.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// super.getDatabaseOrder().getList().get(SubmissionLabel.PATH)
		// .setData(System.getProperty("user.home") + myPath.getFileName());
		// // super.execute();

		//////////////////////////////////////////////////////
	}
}