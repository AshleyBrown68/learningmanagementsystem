package ServerOrder;

import java.io.Serializable;

import control.clientDataReceiving.MessageForClient;
import model.serverStuff.serverCommunication.ReturnOrder;
import model.serverStuff.stuffInTheServer.ServerToolBox;
import orders.SystemOrder;

/**
 * The Class ServerOrder, which represents any order to the server. this
 * involves things like orders to do stuff with the database, and orders to do
 * stuff with messenging (if that gets implemented).
 */
public abstract class ServerOrder extends SystemOrder implements Serializable{

	/** The tool box which the server order will use to perform its actions */
	private ServerToolBox box;
	/** the distribute id of the repective distribute order,*/
	private int distributeID;
	/** the returnOrder for when it wants to return something to the client*/
	protected ReturnOrder forClient;
	/** whether its distributeOrderShouldBeDeleted*/
	private boolean deleteDistribute;

	/**
	 * Instantiates a new server order.
	 */
	public ServerOrder(int distributeID) {
		this.distributeID=distributeID;
		this.forClient=new ReturnOrder(new MessageForClient("",null), distributeID);
		setDeleteDistribute(true);
	}
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -204167145607258263L;

	/**
	 * Gets the tool box.
	 *
	 * @return the box
	 */
	protected ServerToolBox getBox() {
		return box;
	}
	public void setDistributeId(int id) {
		this.distributeID=id;
		this.forClient.setDistributeID(id);
	}

	/**
	 * Sets the box.
	 *
	 * @param box
	 *            the new box
	 */
	public void setBox(ServerToolBox box) {
		this.box = box;
	}
	public int getDistributeID() {
		return distributeID;
	}
	public boolean getDeleteDistribute() {
		return deleteDistribute;
	}
	public void setDeleteDistribute(boolean deleteDistribute) {
		forClient.setDistributeDelete(deleteDistribute);
		this.deleteDistribute = deleteDistribute;
	}
	
}
