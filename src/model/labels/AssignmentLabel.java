package model.labels;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * The Enum AssignmentLabel.
 */
public enum AssignmentLabel implements DataLabel {

	/** The id. */
	ID("ID", 8, 1),
	/** The course id. */
	COURSE_ID("COURSE_ID", 8, 2),
	/** The title. */
	TITLE("TITLE", 50, 3),
	/** The path. */
	PATH("PATH", 100, 4),
	/** The active */
	ACTIVE("ACTIVE", 1, 5),
	/** The due date. */
	DUE_DATE("DUE_DATE", 16, 6),
	/** The extension. */
	EXTENSION("EXTENSION", 5, 7);

	/** The Constant tableName. */
	private static final String tableName = "AssignmentTable";
	/** The label. */
	private final String label;

	/** The column number. */
	private final int columnNumber;

	/** The max size. */
	private final int maxSize;

	/**
	 * Instantiates a new Assignment label.
	 *
	 * @param newLabel
	 *            the new column name
	 * @param maxSize
	 *            the max size
	 * @param columnNumber
	 *            the column number
	 */
	AssignmentLabel(String newLabel, int maxSize, int columnNumber) {
		this.label = newLabel;
		this.columnNumber = columnNumber;
		this.maxSize = maxSize;
	}

	/**
	 * returns the name of the column the label belongs in.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return label;
	}

	/**
	 * returns the number of the column the label belongs in.
	 *
	 * @return the column number
	 */
	public int getColumnNumber() {
		return columnNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getTableName()
	 */
	public String getTableName() {
		return tableName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getMaxSize()
	 */
	public int getMaxSize() {
		return maxSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getLabelBasedOnColumnNumber()
	 */

	@Override
	public DataLabel getLabelBasedOnColumnNumber(int columNumber) {
		for (int i = 0; i < values().length; i++)
			if (values()[i].getColumnNumber() == columNumber) {
				return values()[i];
			}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getAllOfType()
	 */
	@Override
	public ArrayList<DataLabel> getAllForInsertion() {
		ArrayList<DataLabel> list = new ArrayList<DataLabel>();

		DataLabel[] enumArray = this.getClass().getEnumConstants();
		list.addAll(Arrays.asList(enumArray));
		list.remove(ID);
		return list;
	}

	@Override
	public ArrayList<DataLabel> getAllLabels() {
		ArrayList<DataLabel> list = new ArrayList<DataLabel>();

		DataLabel[] enumArray = this.getClass().getEnumConstants();
		list.addAll(Arrays.asList(enumArray));
		return list;
	}
}
