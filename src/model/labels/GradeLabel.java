package model.labels;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * The Enum GradeLabel.
 */
public enum GradeLabel implements DataLabel {

	/** The id. */
	ID("ID", 8, 1),
	/** The assignment id. */
	ASSIGNMENT_ID("ASSIGNMENT_ID", 8, 2),
	/** The student id. */
	STUDENT_ID("STUDENT_ID", 8, 3),
	/** The course id. */
	COURSE_ID("COURSE_ID", 8, 4),
	/** The assignment grade. */
	ASSIGNMENT_GRADE("ASSIGNMENT_GRADE", 3, 5);

	/** The Constant tableName. */
	private static final String tableName = "GradeTable";

	/** The label. */
	private final String label;

	/** The column number. */
	private final int columnNumber;

	/** The max size. */
	private final int maxSize;

	/**
	 * Instantiates a new Grade label.
	 *
	 * @param newLabel
	 *            the new column name
	 * @param maxSize
	 *            the max size
	 * @param columnNumber
	 *            the column number
	 */
	GradeLabel(String newLabel, int maxSize, int columnNumber) {
		this.label = newLabel;
		this.columnNumber = columnNumber;
		this.maxSize = maxSize;
	}

	/**
	 * returns the name of the column the label belongs in.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return label;
	}

	/**
	 * returns the number of the column the label belongs in.
	 *
	 * @return the column number
	 */
	public int getColumnNumber() {
		return columnNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getTableName()
	 */
	public String getTableName() {
		return tableName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getMaxSize()
	 */
	public int getMaxSize() {
		return maxSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getLabelBasedOnColumnNumber()
	 */

	@Override
	public DataLabel getLabelBasedOnColumnNumber(int columNumber) {
		for (int i = 0; i < values().length; i++)
			if (values()[i].getColumnNumber() == columNumber) {
				return values()[i];
			}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getAllOfType()
	 */
	@Override
	public ArrayList<DataLabel> getAllForInsertion() {
		ArrayList<DataLabel> list = new ArrayList<DataLabel>();

		DataLabel[] enumArray = this.getClass().getEnumConstants();
		list.addAll(Arrays.asList(enumArray));
		list.remove(ID);
		return list;
	}
	@Override
	public ArrayList<DataLabel> getAllLabels() {
		ArrayList<DataLabel> list = new ArrayList<DataLabel>();

		DataLabel[] enumArray = this.getClass().getEnumConstants();
		list.addAll(Arrays.asList(enumArray));
		return list;
	}
}
