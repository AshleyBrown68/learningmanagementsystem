package model.labels;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * The Enum CourseLabel, the label which belongs to the course area of the table
 */
public enum CourseLabel implements DataLabel {

	/** The id of the course . */
	ID("ID", 8, 1),
	/** The prof id. */
	PROF_ID("PROF_ID", 8, 2),
	/** The name. */
	NAME("NAME", 50, 3),
	/** The active. */
	ACTIVE("ACTIVE", 1, 4);

	/** The Constant tableName. */
	private static final String tableName = "CourseTable";

	/** The label. */
	private final String label;

	/** The column number. */
	private final int columnNumber;

	/** The max size. */
	private final int maxSize;

	/**
	 * Instantiates a new course label.
	 *
	 * @param newLabel
	 *            the new column name
	 * @param maxSize
	 *            the max size
	 * @param columnNumber
	 *            the column number
	 */
	CourseLabel(String newLabel, int maxSize, int columnNumber) {
		this.label = newLabel;
		this.columnNumber = columnNumber;
		this.maxSize = maxSize;
	}

	/**
	 * returns the name of the column the label belongs in.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return label;
	}

	/**
	 * returns the number of the column the label belongs in.
	 *
	 * @return the column number
	 */
	public int getColumnNumber() {
		return columnNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getTableName()
	 */
	public String getTableName() {
		return tableName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getMaxSize()
	 */
	public int getMaxSize() {
		return maxSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getAllOfType()
	 */

	@Override
	public DataLabel getLabelBasedOnColumnNumber(int columNumber) {
		for (int i = 0; i < values().length; i++)
			if (values()[i].getColumnNumber() == columNumber) {
				return values()[i];
			}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getAllOfType()
	 */
	@Override
	public ArrayList<DataLabel> getAllForInsertion() {
		ArrayList<DataLabel> list = new ArrayList<DataLabel>();

		DataLabel[] enumArray = this.getClass().getEnumConstants();
		list.addAll(Arrays.asList(enumArray));
		list.remove(ID);
		return list;
	}

	@Override
	public ArrayList<DataLabel> getAllLabels() {
		ArrayList<DataLabel> list = new ArrayList<DataLabel>();

		DataLabel[] enumArray = this.getClass().getEnumConstants();
		list.addAll(Arrays.asList(enumArray));
		return list;
	}
}
