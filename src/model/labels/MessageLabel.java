package model.labels;

import java.util.ArrayList;
import java.util.Arrays;

// TODO: Auto-generated Javadoc
/**
 * The Enum MessageLabel.
 */
public enum MessageLabel implements DataLabel {

	/** The id. */
	ID("ID", 8, 1),
	/** who the message is to. */
	TO("TO", 20, 2),
	/** The MESSAGE. */
	MESSAGE("MESSAGE", 100, 3),
	/** who the message was from. */
	FROM("FROM", 20, 4),
	/**The Class Which the MessageBelongsTo	 */
	CLASSID("CLASSID",8,5);

	/** The Constant tableName. */
	private static final String tableName = "Messages";

	/** The label. */
	private final String label;

	/** The column number. */
	private final int columnNumber;

	/** The max size. */
	private final int maxSize;

	/**
	 * Instantiates a new user label.
	 *
	 * @param newLabel
	 *            the new column name
	 * @param maxSize
	 *            the max size
	 * @param columnNumber
	 *            the column number
	 */
	MessageLabel(String newLabel, int maxSize, int columnNumber) {
		this.label = newLabel;
		this.columnNumber = columnNumber;
		this.maxSize = maxSize;
	}

	/**
	 * returns the name of the column the label belongs in.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return label;
	}

	/**
	 * returns the number of the column the label belongs in.
	 *
	 * @return the column number
	 */
	public int getColumnNumber() {
		return columnNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getTableName()
	 */
	public String getTableName() {
		return tableName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getMaxSize()
	 */
	public int getMaxSize() {
		return maxSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getLabelBasedOnColumnNumber()
	 */

	@Override
	public DataLabel getLabelBasedOnColumnNumber(int columNumber) {
		for (int i = 0; i < values().length; i++)
			if (values()[i].getColumnNumber() == columNumber) {
				return values()[i];
			}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see model.labels.DataLabel#getAllOfType()
	 */
	@Override
	public ArrayList<DataLabel> getAllForInsertion() {
		ArrayList<DataLabel> list = new ArrayList<DataLabel>();

		DataLabel[] enumArray = this.getClass().getEnumConstants();
		list.addAll(Arrays.asList(enumArray));
		list.remove(ID);
		list.remove(TO);
		System.out.println("list size: "+list.size());
		return list;
	}
	@Override
	public ArrayList<DataLabel> getAllLabels() {
		ArrayList<DataLabel> list = new ArrayList<DataLabel>();

		DataLabel[] enumArray = this.getClass().getEnumConstants();
		list.addAll(Arrays.asList(enumArray));
		return list;
	}

}
