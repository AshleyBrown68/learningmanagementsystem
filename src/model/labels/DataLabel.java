package model.labels;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The Interface DataLabel, which is supposed to be inherited by all of the labels. This class represents a group of enums, called "Labels"
 * which represents different columns in the database. These labels will be able to give information about the colum/table it belongs to, 
 * ie get the column name, column number, tablename etc.  
 * REQUIRES the primary key to be in the first column of the database. 
 */
public interface DataLabel extends Serializable {
	
	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public abstract String getColumnName();

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public abstract String getTableName();

	/**
	 * Gets the column number.
	 *
	 * @return the column number
	 */
	public abstract int getColumnNumber();
	
	/**
	 * Gets the max size.
	 *
	 * @return the max size
	 */
	public abstract int getMaxSize();
	
	/**
	 * gets a datalabel of the same type based on the column it resides in.  
	 * @return a datalabel of the same type based on the column it resides in.
	 */
	public abstract DataLabel getLabelBasedOnColumnNumber(int columNumber) ;
	public abstract ArrayList<DataLabel> getAllLabels();

	ArrayList<DataLabel> getAllForInsertion();
}
