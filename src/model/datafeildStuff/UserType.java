package model.datafeildStuff;

// TODO: Auto-generated Javadoc
/**
 * The Enum UserType, which represents the type of user, ie if the user is a professor or student
 */
public enum UserType {

	/** The student. */
	STUDENT("S"), /** The professor . */
 PROFESSOR("P");
	
	/** The label of the type of user */
	private final String label;

	/**
	 * Instantiates a new user type.
	 *
	 * @param newLabel the new label of the type. 
	 */
	UserType(String newLabel) {
		this.label = newLabel;
	}

	/**
	 * Gets the type of user
	 * @return the type of user
	 */
	public String getType() {
		return label;
	}
}
