package model.datafeildStuff;

import java.io.Serializable;
import java.util.ArrayList;

import model.labels.DataLabel;

// TODO: Auto-generated Javadoc
/**
 * The Class Rebecca_DataFeildList, which is basically a custom vector for holding
 * DataFeilds. The difference between this and an ordinary vector, is that it is
 * custom built to hold data for only a single row of the database. To maintain
 * this, it does error checking it checks if there are duplicate labels in the
 * vector, and whether all of the labels belong to the same table. it also
 * checks if the vector is to big.
 */
public class Rebecca_DataFeildList implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8160451334623592865L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Rebecca_DataFeildList [list=" + list + "]";
	}

	/** The list. */
	private ArrayList<DataFeild> list;

	/**
	 * Instantiates a new data feild list.
	 */
	public Rebecca_DataFeildList() {
		list = new ArrayList<DataFeild>();
	}

	/**
	 * adds the inputed DataFeild to the list.
	 * 
	 * @param feildAdded
	 *            the DataFeild which will be added to the list
	 * @throws IllegalArgumentException
	 *             when the inputed DataFeild has the same label as another member
	 *             of the list, or the inputed DataFeild belongs to a different
	 *             table
	 */
	public void add(DataFeild feildAdded) throws IllegalArgumentException {
		if (list.isEmpty()) {
			list.add(feildAdded);
			return;
		}
		checkNewInput(feildAdded);
		list.add(feildAdded);
	}

	/**
	 * gets the field corresponding to the index.
	 * 
	 * @param i
	 *            the index of the field being retrieved
	 * @return the DataFeild with the inputed index
	 */
	public DataFeild get(int i) {
		return list.get(i);
	}

	/**
	 * Deletes the dataFeild at the index from the table
	 *
	 * @param i
	 *            the i
	 */
	public void delete(int i) {
		list.remove(i);
	}

	/**
	 * deletes a DataFeild in the list which has the inputed label.
	 *
	 * @param label
	 *            the label of the DataFeild which will be deleted
	 */
	public void delete(DataLabel label) {
		for (int i = 0; i < list.size(); i++) {
			if (label == list.get(i).getLabel()) {
				delete(i);
				return;
			}
		}
	}

	/**
	 * gets the member based on the label inputed. ie if you input userLabel.ID, it
	 * would get the the DataFeild of the id. Warning: if it cant find a DataFeild
	 * with that label, it will return null;
	 * 
	 * @param label
	 *            the label of the DataFeild you want to get
	 * @return the DataFeild which has the inputed label
	 */
	public DataFeild get(DataLabel label) {
		for (int i = 0; i < list.size(); i++) {
			if (label == list.get(i).getLabel()) {
				return list.get(i);
			}
		}
		return null;
	}

	/**
	 * checks if the list is empty.
	 *
	 * @return if the list is empty
	 */
	public boolean isEmpty() {
		return list.isEmpty();
	}

	/**
	 * checks if the list has one DataFeild for each column of its corresponding
	 * table in the database.
	 *
	 * @return whether the list has one DataFeild for each column of its
	 *         corresponding table in the database
	 */
	public boolean isReadyForInsertion() {
		if (isEmpty()) {
			return false;
		}

		ArrayList<DataLabel> insertList = list.get(0).getLabel().getAllForInsertion();
		if (insertList.size() != list.size()) {
			System.out.println("thinks that sizes are not the same");
			System.out.println(insertList.size() + " " + list.size());

			return false;
		}
		return checkIfElementsMatchInsertion(insertList);

	}

	private boolean checkIfElementsMatchInsertion(ArrayList<DataLabel> insertList) {

		int i, j;
		for (i = 0; i < list.size(); i++) {

			for (j = 0; j < insertList.size() - 1; j++) {
				System.out.println(j + " i:" + i);
				if (list.get(i).getLabel() == insertList.get(j)) {
					break;
				}
			}
			if (list.get(i).getLabel() != insertList.get(j)) {
				System.out.println("thinks that not the same");
				System.out.println(list.get(i).getLabel() + " " + list.get(i).getLabel());
				return false;
			}
		}
		return true;
	}

	/**
	 * checks if an added input can be added to the list.
	 * 
	 * @param toBeChecked
	 *            the new input which will be checked if it is valid
	 */
	private void checkNewInput(DataFeild toBeChecked) {
		checkNewInputsTable(toBeChecked);
		checkIfInputIsAlreadyInList(toBeChecked);
	}

	/**
	 * checks if the inputed parameter has the same table as the first input.
	 * REQUIRES there to be one input in the list already
	 * 
	 * @param toBeChecked
	 *            the DataFeild which will be checked
	 */
	private void checkNewInputsTable(DataFeild toBeChecked) {
		if (toBeChecked.getLabel().getTableName() != list.get(0).getLabel().getTableName()) {
			throw new IllegalArgumentException(
					"error, tried to add a peice of data which belonged in a diffrent table");
		}
	}

	public int size() {
		return list.size();
	}

	/**
	 * checks if any DataFeild in the list has the same label as the inputed
	 * Datafeild.
	 *
	 * @param toBeChecked
	 *            the DataFeild which will be checked
	 */
	private void checkIfInputIsAlreadyInList(DataFeild toBeChecked) {
		for (int i = 0; i < list.size(); i++)
			if (toBeChecked.getLabel() == list.get(i).getLabel()) {
				throw new IllegalArgumentException("error, tried to add a duplicate peice of data");
			}
	}

	public boolean isFull() {
		if (list.isEmpty()) {
			return false;
		}
		return list.size() == list.get(0).getLabel().getAllLabels().size();

	}

}
