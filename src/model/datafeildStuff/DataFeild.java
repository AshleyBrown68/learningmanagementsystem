package model.datafeildStuff;

import java.io.Serializable;

import model.labels.DataLabel;

/**
 * The Class DataFeild, which represents a single field of data in the table. ie
 * what goes in the table. For instance an id of a user
 */
public class DataFeild implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7196015725269776698L;

	/**
	 * The label of the data, which represents the column and table for the data to
	 * go into
	 */
	private DataLabel label;

	/** The data that will be inserted/searched for in the table */
	private String data;

	/**
	 * returns the DataFeilds label, ie the label enum which corresponds to the
	 * datas place in the database.
	 *
	 * @return the datas label representing the data's place in the database
	 */
	public DataLabel getLabel() {
		return label;
	}

	/**
	 * returns the data which will be stored in the database.
	 * 
	 * @return the data which will be store din the database
	 */
	public String getData() {
		return data;
	}

	/**
	 * sets the data which will be stored in the database to the set value.
	 * 
	 * @param newData
	 *            the new value of the data which will be stored in the database
	 */
	public void setData(String newData) {
		this.data = newData;
	}

	/**
	 * constructs the DataFeild object.
	 * 
	 * @param label
	 *            the label representing the datas place in the database
	 * @param data
	 *            the data which will be stored/searched for in the database
	 */
	public DataFeild(DataLabel label, String data) {
		if (data.length() > label.getMaxSize()) {
			throw new IllegalArgumentException("error, your " + label.getColumnName().toLowerCase()
					+ " input is too big, it must be less than " + label.getMaxSize());
		}
		this.label = label;
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return label.getColumnName() + ": " + data;
	}
	
}
