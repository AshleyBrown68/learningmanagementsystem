package model.serverStuff.stuffInTheServer;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import messenger.MessageRoomOrganizer;
import model.serverStuff.serverCommunication.ServerDefaultValues;
/** provides an object of type FinalProjectServer which creates the thread which the database will handle inputs (DataBaseBackend).
 * it also links DatabaseBackend class's sockets with the server sockets, which allows that class to communicate with the client
 * it also creates and stores each DataBaseBackend in a ExecutorService, which then starts its process.
 * @author Kevin Brereton
 *@version 1.0
 *@since march 14 2018
 */

//this was copy pasted from my other project, It most likley wont change too much no matter what we do
public class FinalProjectServer implements ServerDefaultValues{
	/**
	 * the server socket it uses to create the sockets, which link to the client
	 */
	private ServerSocket servSocket;
	/**
	 * the pool of threads, which holds all of the DataBaseBackends
	 */
	private ExecutorService pool;
	
	private MessageRoomOrganizer organizer;
	/**
	 * creates a new FinalProjectServer object, instantiating all members, and initiating the sever with an id of 9091
	 */
	public FinalProjectServer( int i) {
		try {
			servSocket=new ServerSocket(i);
			pool=Executors.newCachedThreadPool();
			
			this.organizer=new MessageRoomOrganizer();
			System.out.println("Server started");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * looks for a client linking to the server, when it finds them, creates a new messengerHandeler
	 * and adds it to the pool.  
	 */
	public void addClient() {
		Socket xSoc = null;
		try {
			 xSoc=servSocket.accept();		
		} catch (IOException e) {
			System.out.println("IO error with accepting a clinet");
			System.exit(1);
			e.printStackTrace();
		}
		pool.execute(new ServerSystemConstructor(xSoc,organizer));
	}

	public static void main(String[] args) {		
		FinalProjectServer server=new FinalProjectServer(ID);
		while(true) {
			server.addClient();
		}
	}
	
}
	
