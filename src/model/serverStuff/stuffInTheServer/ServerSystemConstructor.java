package model.serverStuff.stuffInTheServer;


	import java.io.IOException;

	import java.net.Socket;

import messenger.MessageRoomOrganizer;
import model.serverStuff.serverCommunication.ServerConstructor;
import model.serverStuff.serverCommunication.SocketReceiver;
import model.serverStuff.serverCommunication.SocketSender;


	// TODO: Auto-generated Javadoc
/**
	 * this is a class which constructs the ServerSystem for one client, and starts it.  after this point,
	 * the serverSystem can serve one client
	 * @author brere
	 *
	 */
	public class ServerSystemConstructor implements Runnable{
		
		/** socket for communcicating with client. */
		private Socket aSocket;
		
		/** server receiver for receiving inputs from the client. */
		private SocketReceiver receiver;
		/**
		 * server sender for sending inputs to the client.
		 */
		private SocketSender sender;
		
		/**
		 * Instantiates a new server system constructor.
		 *
		 * @param in the in
		 */
		public ServerSystemConstructor(Socket in,MessageRoomOrganizer organizer) {
				aSocket =in;
				System.out.println("database accepted another client");
				constructServerComponents(in, organizer);
		}
		
		/**
		 * Construct server components.
		 *
		 * @param in the socket it uses to constuct its server components
		 */
		private void constructServerComponents(Socket in,MessageRoomOrganizer organizer){
			ServerRouter router=new ServerRouter(null);
			ServerConstructor constructor=new ServerConstructor(in,router,new ServerErrorDisplayer());
			sender=constructor.getSender();
			ServerToolBox box=new ServerToolBox(constructor.getSender(),organizer);
			router.setBox(box);
			receiver=constructor.getReceiver();
		}
		/**
		 * runs the Server System, ie gets the receiver to send data, and get the system working
		 */
		
		public void run() {
			Thread receiverThread=new Thread(receiver);
			receiverThread.start();
			try {
				receiverThread.join();
			} catch (InterruptedException e) {
				System.out.println("server constructor got interrupted for some reason");
				e.printStackTrace();
			}
			close();
		}

		
		/**
		 * closes all of the nessicary streams.
		 */
		public void close() {
			try {
				System.out.println("thread closed");
				sender.close();
				receiver.close();
				aSocket.close();
			} catch (IOException e) {
				System.out.println("error with closing inputs");
				e.printStackTrace();
			}
		}
	

}
