package model.serverStuff.serverCommunication;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import control.clientDataReceiving.Router;
import view.ErrorDisplayer;

/**
 * class which constructs both pieces of the server, and links the two pieces together.
 * ie it constructs the server sender/receiver. 
 * @author brere
 */
public class ServerConstructor implements ServerDefaultValues {
	
	/** The sender which will be constructed*/
	private SocketSender sender;
	
	/** The receiver which will be constructed */
	private SocketReceiver receiver;
	
	/** The socket. */
	private Socket socket;
	
	/**
	 * Instantiates a new server constructor using the default values in the interface inServerDefaultValues
	 * @param router the router which the receiver will use to receive inputs
	 */
	public ServerConstructor(Router router,ErrorDisplayer emergancy) {

		try {
			socket = new Socket(CLIENTNAME, ID);
			setupServerComponents( router,emergancy);
		} catch (UnknownHostException e) {
			emergancy.outputErrorMessage("error, unable to connect with known host, exiting");
			
		} catch (IOException e) {
			emergancy.outputErrorMessage("unable to connect with sever, please try again at a later date");
		}
	}

	/**
	 * Instantiates a new server constructor using the inputed socket
	 * @param sock the socket for the server sender/receiver
	 * @param router the router for the serverReceiver to send inputs to
	 */
	public ServerConstructor(Socket sock, Router router,ErrorDisplayer displayer) {
		socket = sock;
		setupServerComponents( router, displayer);
	}

	/**
	 * sets up the server components.  ie the sener/receiver
	 * @param socket the socket which will be used to send/receive inputs
	 * @param router the router which will be used to send/receive inputs
	 * @param emergancy 
	 */ 
	private void setupServerComponents( Router router, ErrorDisplayer emergancy) {
		try {
			sender = new SocketSender(socket.getOutputStream(),emergancy);
			receiver = new SocketReceiver(socket.getInputStream(), router,emergancy);
			sender.setReceiver(receiver);
		} catch (UnknownHostException e) {
			emergancy.outputErrorMessage("error, unable to connect with known host, exiting");
		} catch (IOException e) {
			emergancy.outputErrorMessage("unable to connect with sever, please try again at a later date");
		}
	}

	/**
	 * Gets the sender.
	 *
	 * @return the sender
	 */
	public SocketSender getSender() {
		return sender;
	}

	/**
	 * Gets the receiver.
	 *
	 * @return the receiver
	 */
	public SocketReceiver getReceiver() {
		return receiver;
	}

}
