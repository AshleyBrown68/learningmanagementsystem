package model.serverStuff.serverCommunication;

import java.io.Serializable;

import control.clientDataReceiving.MessageForClient;
import orders.guiOrder.DistributeOrder;
import orders.guiOrder.GUIOrder;

// TODO: Auto-generated Javadoc
/**
 * The Class ReturnOrder, which is what the gui will receive from the server.
 */
public class ReturnOrder extends GUIOrder implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2289775074049571674L;
	
	/** The order. */
	private DistributeOrder order;
	
	/** The message. */
	private MessageForClient message;
	
	/** The distribute delete. */
	private boolean distributeDelete;
	
	/** The distribute ID. */
	private int distributeID;

	/**
	 * Instantiates a new return order, 
	 * 
	 * @param message the message
	 * @param distributeID the distribute ID
	 */
	public ReturnOrder(MessageForClient message, int distributeID) {
		this.message = message;
		this.distributeID = distributeID;
	}

	/**
	 * Sets the distribute ID.
	 *
	 * @param distributeID the new distribute ID
	 */
	public void setDistributeID(int distributeID) {
		this.distributeID = distributeID;
	}

	/**
	 * Sets the distribute order.
	 *
	 * @param order the new distribute order
	 */
	public void setDistributeOrder(DistributeOrder order) {
		this.order = order;
	}

	/* (non-Javadoc)
	 * @see orders.SystemOrder#execute()
	 */
	public void execute() {
		order.setMessage(message);
		order.execute();
	}

	/**
	 * Gets the distribute ID.
	 *
	 * @return the distribute ID
	 */
	public int getDistributeID() {
		return distributeID;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public MessageForClient getMessage() {
		return message;
	}

	/**
	 * Gets the distribute delete.
	 *
	 * @return the distribute delete
	 */
	public boolean getDistributeDelete() {
		return distributeDelete;
	}

	/**
	 * Sets the distribute delete.
	 *
	 * @param distributeDelete the new distribute delete
	 */
	public void setDistributeDelete(boolean distributeDelete) {
		this.distributeDelete = distributeDelete;
	}

}
