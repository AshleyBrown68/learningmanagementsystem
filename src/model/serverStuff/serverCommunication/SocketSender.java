package model.serverStuff.serverCommunication;

import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import orders.ErrorLocation;
import orders.SystemOrder;
import orders.guiOrder.DistributeOrder;
import view.ErrorDisplayer;

/**
 * this is the class which is in charge of sending orders to the other system.  ie the client sending
 * orders to the server, and the server sending orders to the client. 
 */

public class SocketSender implements ServerDefaultValues {
	/**
	 * 
	 * the object output stream which will print to the socket
	 */
	private ObjectOutputStream printer;
	/**
	 * The receiver which will receive error messages from the serverSender, if nessicary
	 */
	private SocketReceiver receiver;
	/** error displayer, for displaying errors*/
	private ErrorDisplayer errorDisplay;
	/**
	 * creates the object, accepting an outputStream to print it to
	 * 
	 * @param stream
	 *            the stream it will print objects to
	 * @param emergancy 
	 * @throws IOException 
	 */

	public SocketSender(OutputStream stream, ErrorDisplayer emergancy) throws IOException {
			printer = new ObjectOutputStream(stream);
			errorDisplay=emergancy;
			printer.flush();
			printer.reset();
	}

	/**
	 * sends the ServerReqeust to the server.  note that it will not automatically send it to the
	 * receiver if it is an error message.  this is because sometimes you want to send an error messsage
	 * to the other system
	 * @param request  the request which will be sent to the server
	 */
	public void send(SystemOrder request) {
		
		try {
			printer.writeObject(request);
			printer.flush();
			printer.reset();
			System.out.println("sent");
		} catch (InvalidClassException e) {
			handleError(request,"error, the programers messed up and tried to send an invalid class");
		} catch (IOException e) {
			e.printStackTrace();
			handleError(request,"error, the connection to the server has been compromized, please try again later");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Handles an error, by setting the requests error message, to the inputed error message,  then it
	 * sends the error message to the serverReceiver.
	 * @param request the request which will be turned into an error message
	 * @param errorMessage the error message which will be told to the user
	 */
	private void handleError(SystemOrder request,String errorMessage) {
		request.addError(errorMessage,ErrorLocation.GUI);
		sendErrorMessage(request);
	}
	/**
	 * message for linking sender to a receiver, ONLY for sending error messages. you cannot make a receiver
	 * receive stuff from only one sender, as thats not how sockets work.  
	 * @param receiver the receiver that will receive error messages from this sender
	 */
	
	public void storeDistributeOrder(DistributeOrder order) {
		this.receiver.addDistributeOrder(order);
	}
	/**
	 * sends an error message to the ErrorDisplayer, to display the SystemOrders error message
	 */
	public void sendErrorMessage(SystemOrder errrorOrder) {
		errorDisplay.outputErrorMessage(errrorOrder.getFirstError().getErrorMessage());
	}
	/**
	 * sets the receiver for to the argument
	 * @param receiver
	 */
	protected void setReceiver(SocketReceiver receiver) {
		this.receiver = receiver;
	}

	/**
	 * closes the serverSender
	 * @throws IOException when closing is unsuccessful
	 */
	public void close() throws IOException {
		printer.close();
	}

}
