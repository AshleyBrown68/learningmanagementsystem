package control.dataGUIOrders;

import java.util.ArrayList;

import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import orders.databaseOrders.ModifyOrder;
/**
 * an order for the gui.  When executed, it will modify a row in the database
 * @author brere
 *
 */
public class ModifyGUIOrder extends DataGUIOrder{

	/** The order. */
	private ModifyOrder order;
	/**
	 * Instantiates a new modify order.
	 *
	 * @param send the send
	 * @param packager the packager
	 */
	public ModifyGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new ModifyOrder();
	}

	/**
	 * when an action is performed, it retrieves a dataFeildList from itself, then sends it to the 
	 * packager
	 */
	

	@Override
	public void execute() {
		
		DataFeildList data = list;
		if (!data.isFull()) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input all data feilds while inserting", list));
			return;
		}
		
		order.setData(data);
		super.sendToPackager(order);
		
	}

}
