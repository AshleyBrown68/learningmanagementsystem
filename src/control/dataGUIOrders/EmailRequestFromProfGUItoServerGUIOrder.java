package control.dataGUIOrders;

import java.util.ArrayList;

import ServerOrder.EmailRequestFromProfGUItoServer;
import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
import model.labels.StudentEnrollmentLabel;
import orders.databaseOrders.SearchOrder;

/** gui request for a prof emailing to his students */

public class EmailRequestFromProfGUItoServerGUIOrder extends DataGUIOrder {

	/** The order. */
	private SearchOrder order;
	/** title of email */
	private String title;
	/** message of email */
	private String message;

	/**
	 * Instantiates a new insert listener.
	 *
	 * @param send
	 *            the send
	 * @param packager
	 *            the packager
	 */
	public EmailRequestFromProfGUItoServerGUIOrder(DataFeildList list, ClientPackager packager, String title,
			String message) {
		super(list, packager);
		order = new SearchOrder();
		this.title = title;
		this.message = message;
	}

	/**
	 * when an action is performed, it retrieves a dataFeildList from the
	 * superclass, then sends it to the packager. it sends its order to the
	 * packager.
	 */

	@Override
	public void execute() {
		DataFeildList data = list;
		// if (!(data.get(0).getLabel() instanceof CourseLabel)) {
		if (!(data.get(0).getLabel() instanceof StudentEnrollmentLabel)) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input a StudentEnrollment label", list));
			System.out.println("insideGUIOrder");
			return;
		}
		order.setData(data);
		super.sendDataBaseServerOrder(new EmailRequestFromProfGUItoServer(order, title, message));
	}

}
