package control.dataGUIOrders;

import ServerOrder.DatabaseServerOrder;
import ServerOrder.ServerOrder;
import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import orders.databaseOrders.DataOrder;
import orders.databaseOrders.InsertOrder;
import orders.guiOrder.GUIOrder;
import orders.messengerOrders.SendMessageOrder;

/**
 * this is an abstract class meant to represent all dataGUIOrders.  when these are executed on the 
 * gui side, it will send itself to the server, and execute the command specified by the sublcass. 
 */
 public abstract class DataGUIOrder extends GUIOrder {
	
	

	/** The sender which it will use to send data. */
	protected DataFeildList  list;
	
	/** The packager which it will use to send data to the server. */
	private ClientPackager packager;

	/**
	 * Instantiates a new data listener.
	 * @param send the sender
	 * @param packager the packager
	 */
	public DataGUIOrder(DataFeildList list, ClientPackager packager) {
		this.packager = packager;
		this.list = list;
	}


	/**
	 * Sends data to packager
	 * @param order the order which will be sent to the packager
	 */
	
	protected void sendToPackager(DataOrder order) {
		packager.sendToserver(order);
	}
	protected void sendDataBaseServerOrder(ServerOrder serverOrder) {
		packager.sendSpecialDataBaseRequest(serverOrder);
	}

	/**
	 * Sends error message to the client
	 * @param messageForClient the message for client
	 */
	protected void sendErrorMessage(MessageForClient messageForClient) {
		packager.sendErrorMessage(messageForClient);
	}
}
