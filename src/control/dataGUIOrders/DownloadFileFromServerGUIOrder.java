package control.dataGUIOrders;

import java.util.ArrayList;

import ServerOrder.GUIRequestForReceivingFileFromServer;
import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import model.labels.SubmissionLabel;
import orders.databaseOrders.SearchOrder;
/**
 * a order which is meant to be used by the gui, when executed, it downloads a file from the server.
 * @author brere
 */
public class DownloadFileFromServerGUIOrder extends DataGUIOrder{

	/** The order. */
	private SearchOrder order;
	
	public DownloadFileFromServerGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new SearchOrder();
	}

	/**
	 * when an action is performed, it asks the server to download a file for it.
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!(data.get(0).getLabel() instanceof SubmissionLabel)) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input a submission label", list));
			return;
		}
		order.setData(data);
		super.sendDataBaseServerOrder(new GUIRequestForReceivingFileFromServer(order));
	}

}
