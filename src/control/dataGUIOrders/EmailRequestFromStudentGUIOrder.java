package control.dataGUIOrders;

import java.util.ArrayList;

import ServerOrder.EmailRequestFromStudentGUItoServer;
import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
import orders.databaseOrders.SearchOrder;
/** email request for student to email prof */
public class EmailRequestFromStudentGUIOrder extends DataGUIOrder{

	/** The order. */
	private SearchOrder order;
	/**title of email*/
	private String title;
	/** message in email*/
	private String message;
	/**
	 * Instantiates a new insert listener.
	 *
	 * @param send the send
	 * @param packager the packager
	 */
	public EmailRequestFromStudentGUIOrder(DataFeildList list, ClientPackager packager,String title,String message) {
		super(list, packager);
		order = new SearchOrder();
		this.title=title;
		this.message=message;
	}

	/**
	 * when executed, it sends the server an order to email the professor.  
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!(data.get(0).getLabel() instanceof CourseLabel)) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input a course label", list));
			return;
		}
		order.setData(data);
		super.sendDataBaseServerOrder(new EmailRequestFromStudentGUItoServer(order,title,message));
	}

}

