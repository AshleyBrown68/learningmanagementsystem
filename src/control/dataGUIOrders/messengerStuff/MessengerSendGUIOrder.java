package control.dataGUIOrders.messengerStuff;

import java.util.ArrayList;

import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.DataGUIOrder;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;
import orders.messengerOrders.SendMessageOrder;
/**
 * this is a class which is meant to be executed gui side. when executed
 * it sends itself to the server, and sends a message to the chatroom.
 * @author brere
 */
public class MessengerSendGUIOrder extends DataGUIOrder{

	public MessengerSendGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
	}

	/**
	 * when executed, it sends the message to the chatroom on the server.  
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!(data.get(0).getLabel() instanceof MessageLabel)) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input a Message label", list));
			return;
		}
		if(!data.isReadyForInsertion()) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input all relevent data feilds to send", list));
			return;
		}
		super.sendDataBaseServerOrder(new SendMessageOrder(data) );
	}

}

