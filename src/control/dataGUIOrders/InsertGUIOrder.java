package control.dataGUIOrders;


import java.util.ArrayList;

import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import orders.databaseOrders.InsertOrder;

/**
 * 
 *the class which is a gui order, that when executed, will insert the data inputed in the dataFeildList into the
 *database.
 */
public class InsertGUIOrder extends DataGUIOrder{
	/** The order. */
	private InsertOrder order;
	/**
	 * Instantiates a new insert order.
	 *
	 * @param send the send
	 * @param packager the packager
	 */
	public InsertGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new InsertOrder();
	}

	/**
	 * when executed, will insert its data into the database
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!data.isReadyForInsertion()) {

			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input all apropriate data feilds while Inserting", list));
			return;
		}
		
		order.setData(data);

		super.sendToPackager(order);

	}
}
