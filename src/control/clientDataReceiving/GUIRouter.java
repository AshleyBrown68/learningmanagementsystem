package control.clientDataReceiving;

import orders.ErrorLocation;
import orders.SystemOrder;
import orders.guiOrder.GUIOrder;
import view.View;

// TODO: Auto-generated Javadoc
/**
 * The Class GUIRouter, which is a router for the gui.  it receives system orders, and executes them
 * with protocols in place for the gui.
 */
public class GUIRouter extends Router {

	View view;
	
	public GUIRouter(View view) {
		this.view = view;
	}
	/** 
	 * executes a SystemOrder, with the protocols in place of the gui.
	 */
	public synchronized  void executeOrder(SystemOrder order) {
		if (!(order instanceof GUIOrder)) {
			order.addError("error, an order which is not a gui order was executed at the gui location", ErrorLocation.GUI);
		}
		GUIOrder gui;
//		if(order instanceof GUIOrder) {
			gui = (GUIOrder)order;
			gui.setView(view);
			
//		}
			
//			if(gui==null) {
//				System.out.println("recived null order");
//				return;
//			}
//			gui.execute();
		
		super.executeOrder(gui);

	}
}
