package control.clientDataReceiving;

import java.io.Serializable;
import java.util.ArrayList;

import model.datafeildStuff.DataFeildList;
// TODO: Auto-generated Javadoc
/**
 * this is the class which will be received by the receivers.  ie when the data is sent to the server, this
 * is the type of data the client receives back.
 * @author brere
 */
public class MessageForClient implements Serializable {
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3374952659323210016L;
	
	/** The message. */
	private String message;
	
	/** The list. */
	private ArrayList<DataFeildList> list;
	
	/**
	 * Instantiates a new message for client.
	 *
	 * @param message the message
	 * @param list the list
	 */
	public MessageForClient(String message,ArrayList<DataFeildList> list) {
		this.message = message;
		this.list=list;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public ArrayList<DataFeildList> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list2 the new list
	 */
	public void setList(ArrayList<DataFeildList> list2) {
		this.list = list2;
	}
}
