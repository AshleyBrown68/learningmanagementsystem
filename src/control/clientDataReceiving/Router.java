package control.clientDataReceiving;



import orders.SystemOrder;
/**
 * class for executing any system order. wont work with databaseOrders
 * @author brere
 */
public class Router  {
	

	/**
	 * executes a system order.
	 * @param order the order which will be executed
	 */
	public synchronized  void executeOrder(SystemOrder order) {
		if(order==null) {
			System.out.println("recived null order");
			return;
		}
		order.execute();
	}
}
