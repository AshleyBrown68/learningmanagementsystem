package control.dataSending;

import java.io.Serializable;
import java.util.ArrayList;

import ServerOrder.DatabaseServerOrder;
import ServerOrder.ServerOrder;
import control.clientDataReceiving.MessageForClient;
import model.serverStuff.serverCommunication.SocketSender;
import control.clientDataReceiving.DataReceiver;
import orders.ErrorLocation;
import orders.databaseOrders.DataOrder;
import orders.guiOrder.DistributeOrder;

/**
 * The Class Packager, which is meant to be the bridge between the client system, and the server system.
 * it does this through packaging inputs of the client system to the server system.  Each packager has a list of
 * receivers, which will receive the the outcome of the server request.
 */
public class ClientPackager implements Serializable{

	private static final long serialVersionUID = -1678998655157623324L;

	/** The receiver list, which holds all receivers which will receive data from the database */
	private ArrayList<DataReceiver> receiverList;

	/** The server. */
	private SocketSender server;

	/**
	 * Instantiates a new packager
	 * @param server the server which it will send stuff to
	 */
	public ClientPackager(SocketSender server) {
		this.server = server;
		receiverList = new ArrayList<DataReceiver>();
	}

	/**
	 * Send sends the order to the server.  
	 * @param order the order which will be sent
	 */
	public void sendToserver(DataOrder order) {
		
		DatabaseServerOrder servRequest = createDataBaseServerOrder(order);
		
		server.send(servRequest);
	}
	/**
	 * sends the inputed server request to the server
	 * @param serverOrder the  serverOrder which will be sent to the server
	 */
	public void sendSpecialDataBaseRequest(ServerOrder serverOrder) {
		setupSpecialDataBaseRequest(serverOrder);
		server.send(serverOrder);
	}
	/**
	 * sets up the serverOrder to be sent to the Server
	 * @param serverOrder
	 */
	private void setupSpecialDataBaseRequest(ServerOrder serverOrder) {
		DistributeOrder distRequest = new DistributeOrder(receiverList);
		server.storeDistributeOrder(distRequest);
		serverOrder.setDistributeId(distRequest.getId());
	}

	/**
	 * Creates the order for server.
	 *
	 * @param order the order which the server will execute
	 * @return the database server order 
	 */
	
	private DatabaseServerOrder createDataBaseServerOrder(DataOrder order) {
		
		DistributeOrder distRequest = new DistributeOrder(receiverList);
		server.storeDistributeOrder(distRequest);
		return new DatabaseServerOrder(order,distRequest.getId());
	}

	/**
	 * Adds the receiver to the packagers list of receivers
	 * @param receiver the receiver which will be added to the list of receivers
	 */
	public void addReceiver(DataReceiver receiver) {
		receiverList.add(receiver);
	}
	/**
	 * compleatly clears receivers from the list
	 */
	public void clearReceivers() {
		receiverList=new ArrayList<DataReceiver> ();
	}

	/**
	 * Creates the distribute order out of the message for client
	 * @param message the message which will be added to the distributeOrder
	 * @return the distribute order
	 */
	private DistributeOrder createDistributeOrder(MessageForClient message) {
		DistributeOrder order = new DistributeOrder(receiverList);
		addMessageToDistributeOrder(order, message);
		return order;
	}

	/**
	 * Adds the message to distribute order.
	 *
	 * @param order the order
	 * @param message the message
	 */
	private void addMessageToDistributeOrder(DistributeOrder order, MessageForClient message) {
		order.setMessage(message);
	}

	/**
	 * 
	 * Sends an error message to the serverSender, which will ReRoute it to the router
	 * @param messageForClient the message for client
	 */
	public void sendErrorMessage(MessageForClient messageForClient) {
		DistributeOrder order = createDistributeOrder(messageForClient);
		order.addError(messageForClient.getMessage(),ErrorLocation.GUI);
		server.sendErrorMessage(order);
	}

	


}
